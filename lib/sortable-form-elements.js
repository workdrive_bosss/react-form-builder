"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sortableElement = _interopRequireDefault(require("./sortable-element"));

var _formPlaceHolder = _interopRequireDefault(require("./form-place-holder"));

var _formElements = _interopRequireDefault(require("./form-elements"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TextInput = _formElements["default"].TextInput,
    NumberInput = _formElements["default"].NumberInput,
    TextArea = _formElements["default"].TextArea,
    Dropdown = _formElements["default"].Dropdown,
    Checkboxes = _formElements["default"].Checkboxes,
    DatePicker = _formElements["default"].DatePicker,
    RadioButtons = _formElements["default"].RadioButtons,
    FilePicker = _formElements["default"].FilePicker,
    Email = _formElements["default"].Email,
    Phone = _formElements["default"].Phone;
var FormElements = {};
FormElements.TextInput = (0, _sortableElement["default"])(TextInput);
FormElements.Email = (0, _sortableElement["default"])(Email);
FormElements.Phone = (0, _sortableElement["default"])(Phone);
FormElements.NumberInput = (0, _sortableElement["default"])(NumberInput);
FormElements.TextArea = (0, _sortableElement["default"])(TextArea);
FormElements.Dropdown = (0, _sortableElement["default"])(Dropdown);
FormElements.Checkboxes = (0, _sortableElement["default"])(Checkboxes);
FormElements.DatePicker = (0, _sortableElement["default"])(DatePicker);
FormElements.TimePicker = (0, _sortableElement["default"])(DatePicker);
FormElements.RadioButtons = (0, _sortableElement["default"])(RadioButtons);
FormElements.FilePicker = (0, _sortableElement["default"])(FilePicker);
FormElements.PlaceHolder = (0, _sortableElement["default"])(_formPlaceHolder["default"]);
var _default = FormElements;
exports["default"] = _default;