"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ReactFormGenerator", {
  enumerable: true,
  get: function get() {
    return _form["default"];
  }
});
Object.defineProperty(exports, "ElementStore", {
  enumerable: true,
  get: function get() {
    return _store["default"];
  }
});
exports.ReactFormBuilder = exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDnd = require("react-dnd");

var _reactDndHtml5Backend = _interopRequireDefault(require("react-dnd-html5-backend"));

var _preview = _interopRequireDefault(require("./preview"));

var _toolbar = _interopRequireDefault(require("./toolbar"));

var _form = _interopRequireDefault(require("./form"));

var _store = _interopRequireDefault(require("./stores/store"));

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ReactFormBuilder = /*#__PURE__*/function (_React$Component) {
  _inherits(ReactFormBuilder, _React$Component);

  var _super = _createSuper(ReactFormBuilder);

  function ReactFormBuilder(props) {
    var _this;

    _classCallCheck(this, ReactFormBuilder);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "scrollToBottom", function () {
      var _this$props$scrollAbl = _this.props.scrollAbleParentOffset,
          scrollAbleParentOffset = _this$props$scrollAbl === void 0 ? 0 : _this$props$scrollAbl;
      var root = document.getElementById('form-builder-root');

      for (var i = 0; i < scrollAbleParentOffset; ++i) {
        if (root.parentNode) {
          root = root.parentNode;
        }
      }

      if (root) {
        setTimeout(function () {
          root.scrollTo({
            top: root.clientHeight + 150,
            behavior: 'smooth'
          });
        }, 300);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "_renderPreview", function () {
      return /*#__PURE__*/_react["default"].createElement(_preview["default"], {
        files: [{
          id: 1,
          file_name: 'file_a'
        }],
        manualEditModeOff: _this.manualEditModeOff.bind(_assertThisInitialized(_this)),
        showCorrectColumn: _this.props.showCorrectColumn,
        parent: _assertThisInitialized(_this),
        data: _this.props.data,
        initialData: (0, _constants.convertFromJson)(_this.props.initialData),
        onSaveForm: _this.props.onSaveForm,
        onCancelForm: _this.props.onCancelForm,
        url: _this.props.url,
        saveUrl: _this.props.saveUrl,
        onLoad: _this.props.onLoad,
        onPost: _this.props.onPost,
        editModeOn: _this.editModeOn,
        editMode: _this.state.editMode,
        variables: _this.props.variables,
        editElement: _this.state.editElement,
        isQuiz: _this.props.isQuiz,
        isReadOnly: _this.props.isReadOnly,
        loading: _this.props.loading,
        title: _this.props.title,
        isRequiredMessage: _this.props.isRequiredMessage
      });
    });

    _this.state = {
      editMode: false,
      editElement: null
    };
    return _this;
  }

  _createClass(ReactFormBuilder, [{
    key: "editModeOn",
    value: function editModeOn(data, e) {
      e.preventDefault();
      e.stopPropagation();

      if (this.state.editMode) {
        this.setState({
          editMode: !this.state.editMode,
          editElement: null
        });
      } else {
        this.setState({
          editMode: !this.state.editMode,
          editElement: data
        });
      }
    }
  }, {
    key: "manualEditModeOff",
    value: function manualEditModeOff() {
      if (this.state.editMode) {
        this.setState({
          editMode: false,
          editElement: null
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$isReadOnl = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl === void 0 ? false : _this$props$isReadOnl;
      var toolbarProps = {};
      var style = {
        display: 'flex',
        width: '100%'
      };

      if (isReadOnly) {
        style = Object.assign({
          flex: 1,
          overflowY: 'auto',
          overflowX: 'visible',
          padding: 5
        });
      }

      if (this.props.toolbarItems) {
        toolbarProps.items = this.props.toolbarItems;
      }

      return /*#__PURE__*/_react["default"].createElement(_reactDnd.DndProvider, {
        backend: _reactDndHtml5Backend["default"]
      }, /*#__PURE__*/_react["default"].createElement("div", {
        style: style,
        id: "form-builder-root"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "react-form-builder clearfix ".concat(this.props.isQuiz ? 'quiz-form' : '', " ").concat(isReadOnly ? 'read-only' : '')
      }, /*#__PURE__*/_react["default"].createElement("div", {
        style: {
          display: 'flex',
          flex: 1,
          justifyContent: 'space-between'
        }
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "d-flex flex-column w-75"
      }, this._renderPreview()), !isReadOnly && /*#__PURE__*/_react["default"].createElement(_toolbar["default"], _extends({
        isQuiz: this.props.isQuiz,
        items: this.props.isQuiz ? _constants.quizQuestionItems : _constants.formQuestionItems,
        scrollToBottom: this.scrollToBottom
      }, toolbarProps))))));
    }
  }]);

  return ReactFormBuilder;
}(_react["default"].Component);

exports.ReactFormBuilder = ReactFormBuilder;
var FormBuilders = {};
FormBuilders.ReactFormBuilder = ReactFormBuilder;
FormBuilders.ReactFormGenerator = _form["default"];
FormBuilders.ElementStore = _store["default"];
var _default = FormBuilders;
exports["default"] = _default;