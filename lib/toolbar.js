"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _toolbarDraggableItem = _interopRequireDefault(require("./toolbar-draggable-item"));

var _UUID = _interopRequireDefault(require("./UUID"));

var _store = _interopRequireDefault(require("./stores/store"));

var _constants = require("./constants");

var variables = _interopRequireWildcard(require("../variables"));

var _demobar = _interopRequireDefault(require("./demobar"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Toolbar = /*#__PURE__*/function (_React$Component) {
  _inherits(Toolbar, _React$Component);

  var _super = _createSuper(Toolbar);

  function Toolbar(props) {
    var _this;

    _classCallCheck(this, Toolbar);

    _this = _super.call(this, props);
    var items = _this.props.items ? _this.props.items : _constants.formQuestionItems;
    _this.state = {
      items: items
    };

    _store["default"].subscribe(function (state) {
      return _this.setState({
        store: state
      });
    });

    return _this;
  } // create(item) {
  //   const elementOptions = {
  //     id: ID.uuid(),
  //     element: item.element || item.key,
  //     text: item.name,
  //     editorLabel: item.editorLabel,
  //     static: item.static,
  //     required: false,
  //   };
  //   if (item.static) {
  //     elementOptions.bold = false;
  //     elementOptions.italic = false;
  //   }
  //   if (item.canHaveAnswer) { elementOptions.canHaveAnswer = item.canHaveAnswer; }
  //   if (item.filePicker) { elementOptions.filePicker = item.filePicker; }
  //   if (item.canReadOnly) { elementOptions.readOnly = false; }
  //   if (item.canDefaultToday) { elementOptions.defaultToday = false; }
  //   if (item.content) { elementOptions.content = item.content; }
  //   if (item.href) { elementOptions.href = item.href; }
  //   elementOptions.canHaveDisplayHorizontal = item.canHaveDisplayHorizontal !== false;
  //   elementOptions.canHaveOptionCorrect = item.canHaveOptionCorrect !== false;
  //   elementOptions.canHaveOptionValue = item.canHaveOptionValue !== false;
  //   elementOptions.canPopulateFromApi = item.canPopulateFromApi !== false;
  //   if (item.key === TOOLBOX_ITEMS_KEYS.DATEPICKER || item.key === TOOLBOX_ITEMS_KEYS.TIMEPICKER) {
  //     elementOptions.dateFormat = item.dateFormat;
  //     elementOptions.timeFormat = item.timeFormat;
  //     elementOptions.showTimeSelect = item.showTimeSelect;
  //     elementOptions.showTimeSelectOnly = item.showTimeSelectOnly;
  //   }
  //   // if (item.key === 'FilePicker') {
  //   //   elementOptions._href = item._href;
  //   //   elementOptions.file_path = item.file_path;
  //   // }
  //   if (item.defaultValue) { elementOptions.defaultValue = item.defaultValue; }
  //   if (item.field_name) { elementOptions.field_name = item.field_name + ID.uuid(); }
  //   if (item.label) { elementOptions.label = item.label; }
  //   if (item.placeholder) { elementOptions.placeholder = item.placeholder; }
  //   if (item.options) {
  //     elementOptions.options = _defaultItemOptions(elementOptions.element);
  //   }
  //   console.log(elementOptions);
  //   return elementOptions;
  // }


  _createClass(Toolbar, [{
    key: "_onClick",
    value: function _onClick(item) {
      // ElementActions.createElement(this.create(item));
      var _this$props = this.props,
          _this$props$isQuiz = _this$props.isQuiz,
          isQuiz = _this$props$isQuiz === void 0 ? false : _this$props$isQuiz,
          _this$props$scrollToB = _this$props.scrollToBottom,
          scrollToBottom = _this$props$scrollToB === void 0 ? function () {} : _this$props$scrollToB;
      scrollToBottom();

      _store["default"].dispatch('create', (0, _constants.create)(item, isQuiz));
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props$isQuiz2 = this.props.isQuiz,
          isQuiz = _this$props$isQuiz2 === void 0 ? false : _this$props$isQuiz2;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "react-form-builder-toolbar"
      }, /*#__PURE__*/_react["default"].createElement("h4", null, "".concat(isQuiz ? 'Add new question' : 'Add new option ')), /*#__PURE__*/_react["default"].createElement("ul", null, this.state.items.map(function (item) {
        return /*#__PURE__*/_react["default"].createElement(_toolbarDraggableItem["default"], {
          data: item,
          key: item.key,
          onClick: _this2._onClick.bind(_this2, item),
          onCreate: _this2.create
        });
      })));
    }
  }]);

  return Toolbar;
}(_react["default"].Component);

exports["default"] = Toolbar;