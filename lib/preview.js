"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _immutabilityHelper = _interopRequireDefault(require("immutability-helper"));

var _store = _interopRequireDefault(require("./stores/store"));

var _formElementsEdit = _interopRequireDefault(require("./form-elements-edit"));

var _sortableFormElements = _interopRequireDefault(require("./sortable-form-elements"));

var _constants = require("./constants");

var variables = _interopRequireWildcard(require("../variables"));

var _demobar = _interopRequireDefault(require("./demobar"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var PlaceHolder = _sortableFormElements["default"].PlaceHolder;

var Preview = /*#__PURE__*/function (_React$Component) {
  _inherits(Preview, _React$Component);

  var _super = _createSuper(Preview);

  function Preview(props) {
    var _this;

    _classCallCheck(this, Preview);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "editModeOff", function (e) {
      if (_this.editForm.current && !_this.editForm.current.contains(e.target)) {
        _this.manualEditModeOff();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "manualEditModeOff", function () {
      var editElement = _this.props.editElement;

      if (editElement && editElement.dirty) {
        editElement.dirty = false;
      }

      _this.props.manualEditModeOff();
    });

    _defineProperty(_assertThisInitialized(_this), "validateForm", function () {
      var valid = true;
      var _this$state$data = _this.state.data,
          data = _this$state$data === void 0 ? [] : _this$state$data;

      if (data.length === 0) {
        return false;
      }

      data.forEach(function (currentQuestion) {
        var _currentQuestion$opti = currentQuestion.options,
            options = _currentQuestion$opti === void 0 ? [] : _currentQuestion$opti;
        var isCorrect = false;
        options.forEach(function (currentOption) {
          if (currentOption.correct) {
            isCorrect = true;
          }
        });

        if (!isCorrect) {
          valid = false;
        }
      });
      return valid;
    });

    var onLoad = props.onLoad,
        onPost = props.onPost,
        _props$initialData = props.initialData,
        initialData = _props$initialData === void 0 ? [] : _props$initialData;

    _store["default"].setExternalHandler(onLoad, onPost);

    _this.editForm = /*#__PURE__*/_react["default"].createRef();

    if (initialData && initialData.length > 0) {
      initialData.forEach(function (item) {
        _store["default"].dispatch("create", (0, _constants.create)(item));
      });
    }

    _this.state = {
      data: initialData,
      answer_data: {},
      showCancelModal: false,
      showModal: false,
      deleteItem: null,
      invalidForm: false
    };
    _this.seq = 0;

    var onUpdate = _this._onChange.bind(_assertThisInitialized(_this));

    _store["default"].subscribe(function (state) {
      return onUpdate(state.data);
    });

    _this.moveCard = _this.moveCard.bind(_assertThisInitialized(_this));
    _this.insertCard = _this.insertCard.bind(_assertThisInitialized(_this));
    _this.cancelForm = _this.cancelForm.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Preview, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      // console.log("newProps", nextProps);
      if (this.props.data !== nextProps.data) {
        _store["default"].dispatch("updateOrder", nextProps.data);
      } // if (this.props.initialData !== nextProps.initialData) {
      //   console.log('newProps-inital', nextProps)
      //   nextProps.initialData.forEach((item) => {
      //     store.dispatch('create', create(item));
      //   });
      //   this.setState = ({
      //     data: nextProps.initialData,
      //   });
      // }

    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          data = _this$props.data,
          url = _this$props.url,
          saveUrl = _this$props.saveUrl,
          initialData = _this$props.initialData;

      _store["default"].dispatch("load", {
        loadUrl: url,
        saveUrl: saveUrl,
        data: initialData || []
      });

      document.addEventListener("mousedown", this.editModeOff);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener("mousedown", this.editModeOff);
    }
  }, {
    key: "_setValue",
    value: function _setValue(text) {
      // return text.replace(/[^A-Z0-9]+/gi, "_").toLowerCase();
      return text;
    }
  }, {
    key: "updateElement",
    value: function updateElement(element) {
      var data = this.state.data;
      var found = false;

      for (var i = 0, len = data.length; i < len; i++) {
        if (element.id === data[i].id) {
          data[i] = element;
          found = true;
          break;
        }
      }

      if (found) {
        this.seq = this.seq > 100000 ? 0 : this.seq + 1;

        _store["default"].dispatch("updateOrder", data);
      }
    }
  }, {
    key: "_onChange",
    value: function _onChange(data) {
      var _this2 = this;

      var answer_data = {};
      data.forEach(function (item) {
        if (item && item.readOnly && _this2.props.variables[item.variableKey]) {
          answer_data[item.field_name] = _this2.props.variables[item.variableKey];
        }
      });
      this.setState({
        data: data,
        answer_data: answer_data
      });
    }
  }, {
    key: "_toggleModalVisibility",
    value: function _toggleModalVisibility(item) {
      this.setState({
        showModal: !this.state.showModal,
        deleteItem: item
      });
    }
  }, {
    key: "cancelForm",
    value: function cancelForm() {
      var _this$state$data2 = this.state.data,
          data = _this$state$data2 === void 0 ? [] : _this$state$data2;

      if (data.length > 0) {
        return this._toggleCancelModalVisibility();
      }

      if (!this.props.onCancelForm) {
        return;
      }

      this.props.onCancelForm();
    }
  }, {
    key: "_toggleCancelModalVisibility",
    value: function _toggleCancelModalVisibility() {
      this.setState({
        showCancelModal: !this.state.showCancelModal
      });
    }
  }, {
    key: "_onDestroy",
    value: function _onDestroy() {
      this._toggleModalVisibility();

      _store["default"].dispatch("delete", this.state.deleteItem);
    }
  }, {
    key: "insertCard",
    value: function insertCard(item, hoverIndex) {
      var data = this.state.data;
      var _this$props$isReadOnl = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl === void 0 ? false : _this$props$isReadOnl;

      if (isReadOnly) {
        return;
      }

      data.splice(hoverIndex, 0, item);
      this.saveData(item, hoverIndex, hoverIndex);
    }
  }, {
    key: "moveCard",
    value: function moveCard(dragIndex, hoverIndex) {
      var _this$props$isReadOnl2 = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl2 === void 0 ? false : _this$props$isReadOnl2;

      if (isReadOnly) {
        return;
      }

      var data = this.state.data;
      var dragCard = data[dragIndex];
      this.saveData(dragCard, dragIndex, hoverIndex);
    } // eslint-disable-next-line no-unused-vars

  }, {
    key: "cardPlaceHolder",
    value: function cardPlaceHolder(dragIndex, hoverIndex) {// Dummy
    }
  }, {
    key: "saveData",
    value: function saveData(dragCard, dragIndex, hoverIndex) {
      var newData = (0, _immutabilityHelper["default"])(this.state, {
        data: {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]]
        }
      });
      this.setState(newData);

      _store["default"].dispatch("updateOrder", newData.data);
    }
  }, {
    key: "getElement",
    value: function getElement(item, index) {
      var SortableFormElement = _sortableFormElements["default"][item.element];
      return /*#__PURE__*/_react["default"].createElement(SortableFormElement, {
        id: item.id,
        seq: this.seq,
        index: index,
        moveCard: this.moveCard,
        insertCard: this.insertCard,
        mutable: false,
        parent: this.props.parent,
        editModeOn: this.props.editModeOn,
        isDraggable: false,
        canDrag: false,
        key: item.id,
        sortData: item.id,
        data: item,
        _onDestroy: this._toggleModalVisibility.bind(this),
        isQuiz: this.props.isQuiz,
        isReadOnly: this.props.isReadOnly
      });
    }
  }, {
    key: "onSaveHandler",
    value: function onSaveHandler() {
      var _this$props2 = this.props,
          _this$props2$isQuiz = _this$props2.isQuiz,
          isQuiz = _this$props2$isQuiz === void 0 ? false : _this$props2$isQuiz,
          _this$props2$loading = _this$props2.loading,
          loading = _this$props2$loading === void 0 ? false : _this$props2$loading;

      if (loading) {
        return;
      }

      if (isQuiz) {
        var isValidForm = this.validateForm();

        if (!isValidForm) {
          this.setState({
            invalidForm: true
          });
          return;
        }

        this.setState({
          invalidForm: true
        });
      }

      if (!this.props.onSaveForm) {
        return;
      }

      this.props.onSaveForm((0, _constants.convertDataToJson)(this.state.data));
    }
  }, {
    key: "onCancelHandler",
    value: function onCancelHandler() {
      this._toggleCancelModalVisibility();

      if (!this.props.onCancelForm) {
        return;
      }

      this.props.onCancelForm();
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var classes = this.props.className;
      var _this$state = this.state,
          showModal = _this$state.showModal,
          deleteItem = _this$state.deleteItem,
          showCancelModal = _this$state.showCancelModal;
      var _this$props3 = this.props,
          _this$props3$isReadOn = _this$props3.isReadOnly,
          isReadOnly = _this$props3$isReadOn === void 0 ? false : _this$props3$isReadOn,
          _this$props3$loading = _this$props3.loading,
          loading = _this$props3$loading === void 0 ? false : _this$props3$loading,
          _this$props3$isRequir = _this$props3.isRequiredMessage,
          isRequiredMessage = _this$props3$isRequir === void 0 ? "" : _this$props3$isRequir;

      if (this.props.editMode) {
        classes += " is-editing";
      }

      var data = this.state.data.filter(function (x) {
        return !!x;
      });
      var items = data.map(function (item, index) {
        return _this3.getElement(item, index);
      });
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "preview-wrapper"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: classes
      }, showModal && /*#__PURE__*/_react["default"].createElement("div", {
        id: "deleteModal",
        className: "modal show d-block",
        role: "dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-content d-flex flex-col"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        className: "modal-title"
      }, "Are you sure you want to delete field \"".concat(deleteItem.label, "\"?")), /*#__PURE__*/_react["default"].createElement("span", null, "This action cannot be undone"), /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-footer"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-default",
        onClick: this._onDestroy.bind(this)
      }, "Delete"), /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-primary",
        "data-dismiss": "modal",
        onClick: function onClick() {
          return _this3._toggleModalVisibility(null);
        }
      }, "Cancel"))))), showCancelModal && data.length > 0 && /*#__PURE__*/_react["default"].createElement("div", {
        id: "deleteModal",
        className: "modal show d-block",
        role: "dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-content d-flex flex-col"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        className: "modal-title"
      }, "Do you want to discard the changes?"), /*#__PURE__*/_react["default"].createElement("span", null, "This action cannot be undone"), /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-footer"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-default",
        onClick: this.onCancelHandler.bind(this)
      }, "Discard"), /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-primary",
        "data-dismiss": "modal",
        onClick: function onClick() {
          return _this3._toggleCancelModalVisibility();
        }
      }, "Cancel"))))), /*#__PURE__*/_react["default"].createElement("div", {
        ref: this.editForm
      }, this.props.editElement !== null && /*#__PURE__*/_react["default"].createElement(_formElementsEdit["default"], {
        showCorrectColumn: this.props.showCorrectColumn,
        files: this.props.files,
        manualEditModeOff: this.manualEditModeOff,
        preview: this,
        element: this.props.editElement,
        updateElement: this.updateElement,
        isQuiz: this.props.isQuiz
      })), items.length > 0 && /*#__PURE__*/_react["default"].createElement("div", {
        className: "Sortable"
      }, items), items.length === 0 && /*#__PURE__*/_react["default"].createElement(PlaceHolder, {
        id: "form-place-holder",
        show: items.length === 0,
        index: items.length,
        text: this.props.isQuiz ? "Add question from right menu" : "Add form option from right menu",
        moveCard: this.cardPlaceHolder,
        insertCard: this.insertCard
      }), !!isRequiredMessage && this.state.invalidForm && items.length === 0 && /*#__PURE__*/_react["default"].createElement("div", {
        color: "#a4262c",
        style: {
          color: "#a4262c"
        },
        className: "mt-5"
      }, isRequiredMessage)), !isReadOnly && /*#__PURE__*/_react["default"].createElement("div", {
        className: "bottomBtnBar"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "btn-container"
      }, /*#__PURE__*/_react["default"].createElement(_demobar["default"], {
        variables: variables,
        isQuiz: this.props.isQuiz,
        title: this.props.title
      }), /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-default",
        onClick: this.cancelForm
      }, "Cancel"), /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-primary ".concat(loading ? "loading" : ""),
        onClick: this.onSaveHandler.bind(this),
        disabled: loading
      }, loading && /*#__PURE__*/_react["default"].createElement("div", {
        "class": "spinner-border",
        role: "status"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        "class": "sr-only"
      }, "Loading...")), "Save"))))));
    }
  }]);

  return Preview;
}(_react["default"].Component);

exports["default"] = Preview;
Preview.defaultProps = {
  showCorrectColumn: false,
  files: [],
  editMode: false,
  editElement: null,
  className: "react-form-builder-preview"
};