"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _xss = _interopRequireDefault(require("xss"));

var _dateFns = require("date-fns");

var _reactSignatureCanvas = _interopRequireDefault(require("react-signature-canvas"));

var _reactBootstrapSlider = _interopRequireDefault(require("react-bootstrap-slider"));

var _reactDatepicker = _interopRequireDefault(require("react-datepicker"));

var _starRating = _interopRequireDefault(require("./star-rating"));

var _headerBar = _interopRequireDefault(require("./header-bar"));

var _reactDropzone = _interopRequireWildcard(require("react-dropzone"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var FormElements = {};
var myxss = new _xss["default"].FilterXSS({
  whiteList: {
    u: [],
    br: [],
    b: [],
    i: [],
    ol: ["style"],
    ul: ["style"],
    li: [],
    p: ["style"],
    sub: [],
    sup: [],
    div: ["style"],
    em: [],
    strong: [],
    span: ["style"]
  }
});

var noData = function noData() {
  return /*#__PURE__*/_react["default"].createElement("i", {
    className: "grey-color"
  }, 'No Answer');
};

var ComponentLabel = function ComponentLabel(props) {
  var hasRequiredLabel = props.data.hasOwnProperty("required") && props.data.required === true && !props.read_only;
  return /*#__PURE__*/_react["default"].createElement("label", {
    className: props.className || ""
  }, /*#__PURE__*/_react["default"].createElement("span", null, props.data.label), hasRequiredLabel && /*#__PURE__*/_react["default"].createElement("span", {
    className: "label-required "
  }, "*"));
};

var ComponentHeader = function ComponentHeader(props) {
  if (props.mutable || props.isReadOnly) {
    return null;
  }

  return /*#__PURE__*/_react["default"].createElement("div", null, props.data.pageBreakBefore && /*#__PURE__*/_react["default"].createElement("div", {
    className: "preview-page-break"
  }, "Page Break"), /*#__PURE__*/_react["default"].createElement(_headerBar["default"], {
    parent: props.parent,
    editModeOn: props.editModeOn,
    data: props.data,
    onDestroy: props._onDestroy,
    onEdit: props.onEdit,
    "static": props.data["static"],
    required: props.data.required
  }));
};

var TextInput = /*#__PURE__*/function (_React$Component) {
  _inherits(TextInput, _React$Component);

  var _super = _createSuper(TextInput);

  function TextInput(props) {
    var _this;

    _classCallCheck(this, TextInput);

    _this = _super.call(this, props);
    _this.inputField = /*#__PURE__*/_react["default"].createRef();
    return _this;
  }

  _createClass(TextInput, [{
    key: "render",
    value: function render() {
      var _this$props$isReadOnl = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl === void 0 ? false : _this$props$isReadOnl;
      var props = {
        placeholder: this.props.data.placeholder
      };
      props.type = "text";
      props.className = "form-control";
      props.name = this.props.data.field_name;

      if (this.props.mutable) {
        props.defaultValue = this.props.defaultValue;
        props.ref = this.inputField;
      }

      var baseClasses = "SortableItem rfb-item";

      if (this.props.read_only) {
        props.disabled = "disabled";
      }

      var value = props.defaultValue;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, this.props), !this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("input", _extends({}, props, {
        placeholder: props.placeholder
      })), this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("div", null, value ? value : noData())));
    }
  }]);

  return TextInput;
}(_react["default"].Component);

var Email = /*#__PURE__*/function (_React$Component2) {
  _inherits(Email, _React$Component2);

  var _super2 = _createSuper(Email);

  function Email(props) {
    var _this2;

    _classCallCheck(this, Email);

    _this2 = _super2.call(this, props);
    _this2.inputField = /*#__PURE__*/_react["default"].createRef();
    return _this2;
  }

  _createClass(Email, [{
    key: "render",
    value: function render() {
      var _this$props$isReadOnl2 = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl2 === void 0 ? false : _this$props$isReadOnl2;
      var props = {
        placeholder: this.props.data.placeholder
      };
      props.type = "text";
      props.className = "form-control";
      props.name = this.props.data.field_name;

      if (this.props.mutable) {
        props.defaultValue = this.props.defaultValue;
        props.ref = this.inputField;
      }

      var baseClasses = "SortableItem rfb-item";
      var value = props.defaultValue;

      if (this.props.read_only) {
        props.disabled = "disabled";
      }

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, this.props), !this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("input", _extends({}, props, {
        placeholder: props.placeholder,
        type: "email"
      })), this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("div", null, value ? value : noData())));
    }
  }]);

  return Email;
}(_react["default"].Component);

var Phone = /*#__PURE__*/function (_React$Component3) {
  _inherits(Phone, _React$Component3);

  var _super3 = _createSuper(Phone);

  function Phone(props) {
    var _this3;

    _classCallCheck(this, Phone);

    _this3 = _super3.call(this, props);
    _this3.inputField = /*#__PURE__*/_react["default"].createRef();
    return _this3;
  }

  _createClass(Phone, [{
    key: "render",
    value: function render() {
      var _this$props$isReadOnl3 = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl3 === void 0 ? false : _this$props$isReadOnl3;
      var props = {
        placeholder: this.props.data.placeholder
      };
      props.type = "text";
      props.className = "form-control";
      props.name = this.props.data.field_name;

      if (this.props.mutable) {
        props.defaultValue = this.props.defaultValue;
        props.ref = this.inputField;
      }

      var baseClasses = "SortableItem rfb-item";
      var value = props.defaultValue;

      if (this.props.read_only) {
        props.disabled = "disabled";
      }

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, this.props), !this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("input", _extends({}, props, {
        placeholder: props.placeholder,
        type: "tel"
      })), this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("div", null, value ? value : noData())));
    }
  }]);

  return Phone;
}(_react["default"].Component);

var NumberInput = /*#__PURE__*/function (_React$Component4) {
  _inherits(NumberInput, _React$Component4);

  var _super4 = _createSuper(NumberInput);

  function NumberInput(props) {
    var _this4;

    _classCallCheck(this, NumberInput);

    _this4 = _super4.call(this, props);
    _this4.inputField = /*#__PURE__*/_react["default"].createRef();
    return _this4;
  }

  _createClass(NumberInput, [{
    key: "render",
    value: function render() {
      var _this$props$isReadOnl4 = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl4 === void 0 ? false : _this$props$isReadOnl4;
      var props = {
        placeholder: this.props.data.placeholder
      };
      props.type = "number";
      props.className = "form-control";
      props.name = this.props.data.field_name;

      if (this.props.mutable) {
        props.defaultValue = this.props.defaultValue;
        props.ref = this.inputField;
      }

      if (this.props.read_only) {
        props.disabled = "disabled";
      }

      var baseClasses = "SortableItem rfb-item";
      var value = props.defaultValue;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, this.props), !this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("input", props), this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("div", null, value ? value : noData())));
    }
  }]);

  return NumberInput;
}(_react["default"].Component);

var TextArea = /*#__PURE__*/function (_React$Component5) {
  _inherits(TextArea, _React$Component5);

  var _super5 = _createSuper(TextArea);

  function TextArea(props) {
    var _this5;

    _classCallCheck(this, TextArea);

    _this5 = _super5.call(this, props);
    _this5.inputField = /*#__PURE__*/_react["default"].createRef();
    return _this5;
  }

  _createClass(TextArea, [{
    key: "render",
    value: function render() {
      var _this$props$isReadOnl5 = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl5 === void 0 ? false : _this$props$isReadOnl5;
      var props = {
        placeholder: this.props.data.placeholder
      };
      props.className = "form-control";
      props.name = this.props.data.field_name;

      if (this.props.read_only) {
        props.disabled = "disabled";
      }

      if (this.props.mutable) {
        props.defaultValue = this.props.defaultValue;
        props.ref = this.inputField;
      }

      var baseClasses = "SortableItem rfb-item";
      var value = props.defaultValue;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, this.props), !this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("input", _extends({}, props, {
        placeholder: props.placeholder
      })), this.props.isPreviewResponses && /*#__PURE__*/_react["default"].createElement("div", null, value ? value : noData())));
    }
  }]);

  return TextArea;
}(_react["default"].Component);

var DatePicker = /*#__PURE__*/function (_React$Component6) {
  _inherits(DatePicker, _React$Component6);

  var _super6 = _createSuper(DatePicker);

  function DatePicker(props) {
    var _this6;

    _classCallCheck(this, DatePicker);

    _this6 = _super6.call(this, props);

    _defineProperty(_assertThisInitialized(_this6), "formatMask", "");

    _defineProperty(_assertThisInitialized(_this6), "handleChange", function (dt) {
      var placeholder;

      if (dt && dt.target) {
        placeholder = dt && dt.target && dt.target.value === "" ? _this6.formatMask.toLowerCase() : ""; // ! format the date once implementing the form submission for bosss
        // const formattedDate = dt.target.value
        //   ? format(dt.target.value, this.formatMask)
        //   : "";

        var formattedDate = dt.target.value ? dt.target.value : "";

        _this6.setState({
          value: formattedDate,
          internalValue: formattedDate,
          placeholder: placeholder
        });
      } else {
        // ! format the date once implementing the form submission for bosss
        // const newState = {
        //   value: dt ? format(dt, this.formatMask) : "",
        //   internalValue: dt,
        //   placeholder,
        // }
        var newState = {
          value: dt ? dt : "",
          internalValue: dt,
          placeholder: placeholder
        };

        _this6.setState(newState);
      }
    });

    _this6.inputField = /*#__PURE__*/_react["default"].createRef();

    _this6.updateFormat(props);

    _this6.state = _this6.updateDateTime(props, _this6.formatMask);
    return _this6;
  }

  _createClass(DatePicker, [{
    key: "updateFormat",
    value: function updateFormat(props) {
      var _props$data = props.data,
          showTimeSelect = _props$data.showTimeSelect,
          showTimeSelectOnly = _props$data.showTimeSelectOnly;
      var dateFormat = showTimeSelect && showTimeSelectOnly ? "" : props.data.dateFormat;
      var timeFormat = showTimeSelect ? props.data.timeFormat : "";
      var formatMask = "".concat(dateFormat, " ").concat(timeFormat).trim();
      var updated = formatMask !== this.formatMask;
      this.formatMask = formatMask;
      return updated;
    }
  }, {
    key: "updateDateTime",
    value: function updateDateTime(props, formatMask) {
      var value;
      var internalValue;
      var defaultToday = props.data.defaultToday;

      if (defaultToday && (props.defaultValue === "" || props.defaultValue === undefined)) {
        // ! format the date once implementing the form submission for bosss
        // value = format(new Date(), formatMask);
        value = Date();
        internalValue = new Date();
      } else {
        value = props.defaultValue;

        if (value === "" || value === undefined) {
          internalValue = undefined;
        } else {
          // ! format the date once implementing the form submission for bosss
          // internalValue = parse(value, this.formatMask, new Date());
          internalValue = value;
        }
      }

      return {
        value: value,
        internalValue: internalValue,
        placeholder: formatMask.toLowerCase(),
        defaultToday: defaultToday
      };
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(props) {
      var formatUpdated = this.updateFormat(props);

      if (props.data.defaultToday !== !this.state.defaultToday || formatUpdated) {
        var state = this.updateDateTime(props, this.formatMask);
        this.setState(state);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$data = this.props.data,
          showTimeSelect = _this$props$data.showTimeSelect,
          showTimeSelectOnly = _this$props$data.showTimeSelectOnly;
      var _this$props$isReadOnl6 = this.props.isReadOnly,
          isReadOnly = _this$props$isReadOnl6 === void 0 ? false : _this$props$isReadOnl6;
      var props = {};
      props.type = "date";
      props.className = "form-control";
      props.name = this.props.data.field_name;
      var readOnly = this.props.data.readOnly || this.props.read_only;
      var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
      var placeholderText = this.formatMask.toLowerCase();

      if (this.props.mutable) {
        props.defaultValue = this.props.defaultValue;
        props.ref = this.inputField;
      }

      var baseClasses = "SortableItem rfb-item";

      if (this.props.isPreviewResponses) {
        var value = this.state.value;
        return /*#__PURE__*/_react["default"].createElement("div", null, value ? value : noData());
      }

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, this.props), /*#__PURE__*/_react["default"].createElement("div", null, readOnly && /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        name: props.name,
        ref: props.ref,
        readOnly: readOnly,
        placeholder: this.state.placeholder,
        value: this.state.value,
        className: "form-control"
      }), iOS && !readOnly && /*#__PURE__*/_react["default"].createElement("input", {
        type: "date",
        name: props.name,
        ref: props.ref,
        onChange: this.handleChange,
        dateFormat: "MM/DD/YYYY",
        placeholder: this.state.placeholder,
        value: this.state.value,
        className: "form-control"
      }), !iOS && !readOnly && /*#__PURE__*/_react["default"].createElement(_reactDatepicker["default"], {
        name: props.name,
        ref: props.ref,
        onChange: this.handleChange // selected={this.state.internalValue}
        ,
        todayButton: "Today",
        className: "form-control",
        isClearable: true // showTimeSelect={showTimeSelect}
        // showTimeSelectOnly={showTimeSelectOnly}
        ,
        dateFormat: this.formatMask,
        placeholderText: placeholderText
      }))));
    }
  }]);

  return DatePicker;
}(_react["default"].Component);

var Checkboxes = /*#__PURE__*/function (_React$Component7) {
  _inherits(Checkboxes, _React$Component7);

  var _super7 = _createSuper(Checkboxes);

  function Checkboxes(props) {
    var _this7;

    _classCallCheck(this, Checkboxes);

    _this7 = _super7.call(this, props);

    _defineProperty(_assertThisInitialized(_this7), "error", '');

    _this7.options = {};
    return _this7;
  }

  _createClass(Checkboxes, [{
    key: "render",
    value: function render() {
      var _this8 = this;

      var self = this;
      var _this$props = this.props,
          _this$props$isQuiz = _this$props.isQuiz,
          isQuiz = _this$props$isQuiz === void 0 ? false : _this$props$isQuiz,
          _this$props$isReadOnl7 = _this$props.isReadOnly,
          isReadOnly = _this$props$isReadOnl7 === void 0 ? false : _this$props$isReadOnl7;
      var classNames = "custom-control custom-checkbox";

      if (this.props.data.inline) {
        classNames += " option-inline";
      }

      this.error = '';

      if (isQuiz) {
        var inCorrect = true;
        this.props.data.options.forEach(function (currentOption) {
          if (currentOption.correct) {
            inCorrect = false;
          }
        });

        if (inCorrect) {
          this.error = 'No correct answer is picked';
        }
      }

      var baseClasses = "SortableItem rfb-item";
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, _extends({
        className: "form-label"
      }, this.props)), this.props.data.options.map(function (option) {
        var this_key = "preview_".concat(option.key);
        var props = {};
        props.name = "option_".concat(option.key);
        props.type = "checkbox";
        props.value = option.value;

        if (self.props.mutable) {
          props.defaultChecked = self.props.defaultValue !== undefined && self.props.defaultValue.indexOf(option.key) > -1;
        }

        if (_this8.props.read_only) {
          props.disabled = "disabled";
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: classNames,
          key: this_key
        }, /*#__PURE__*/_react["default"].createElement("input", _extends({
          id: "fid_" + this_key,
          className: "custom-control-input",
          ref: function ref(c) {
            if (c && self.props.mutable) {
              self.options["child_ref_".concat(option.key)] = c;
            }
          }
        }, props)), /*#__PURE__*/_react["default"].createElement("label", {
          className: "custom-control-label",
          htmlFor: "fid_" + this_key
        }, option.text), option.correct && /*#__PURE__*/_react["default"].createElement("i", {
          style: {
            color: 'green'
          },
          className: "ml-3 fas fa-check-circle"
        }));
      }), this.error !== '' && /*#__PURE__*/_react["default"].createElement("label", {
        className: "mt-1 text-danger"
      }, this.error)));
    }
  }]);

  return Checkboxes;
}(_react["default"].Component);

var RadioButtons = /*#__PURE__*/function (_React$Component8) {
  _inherits(RadioButtons, _React$Component8);

  var _super8 = _createSuper(RadioButtons);

  function RadioButtons(props) {
    var _this9;

    _classCallCheck(this, RadioButtons);

    _this9 = _super8.call(this, props);
    _this9.options = {};
    return _this9;
  }

  _createClass(RadioButtons, [{
    key: "render",
    value: function render() {
      var _this10 = this;

      var self = this;
      var _this$props2 = this.props,
          _this$props2$isQuiz = _this$props2.isQuiz,
          isQuiz = _this$props2$isQuiz === void 0 ? false : _this$props2$isQuiz,
          _this$props2$isReadOn = _this$props2.isReadOnly,
          isReadOnly = _this$props2$isReadOn === void 0 ? false : _this$props2$isReadOn;
      var classNames = "custom-control custom-radio";

      if (this.props.data.inline) {
        classNames += " option-inline";
      }

      var baseClasses = "SortableItem rfb-item";
      var error = '';

      if (isQuiz) {
        var inCorrect = true;
        this.props.data.options.forEach(function (currentOption) {
          if (currentOption.correct) {
            inCorrect = false;
          }
        });

        if (inCorrect) {
          error = 'No correct answer is picked';
        }
      }

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: baseClasses
      }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _extends({}, this.props, {
        isReadOnly: isReadOnly
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, _extends({
        className: "form-label"
      }, this.props)), this.props.data.options.map(function (option) {
        var this_key = "preview_".concat(option.key);
        var props = {};
        props.name = self.props.data.field_name;
        props.type = "radio";
        props.value = option.value;

        if (self.props.mutable) {
          props.defaultChecked = self.props.defaultValue !== undefined && (self.props.defaultValue.indexOf(option.key) > -1 || self.props.defaultValue.indexOf(option.value) > -1);
        }

        if (_this10.props.read_only) {
          props.disabled = "disabled";
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: classNames,
          key: this_key
        }, /*#__PURE__*/_react["default"].createElement("input", _extends({
          id: "fid_" + this_key,
          className: "custom-control-input",
          ref: function ref(c) {
            if (c && self.props.mutable) {
              self.options["child_ref_".concat(option.key)] = c;
            }
          }
        }, props)), /*#__PURE__*/_react["default"].createElement("label", {
          className: "custom-control-label",
          htmlFor: "fid_" + this_key
        }, option.text), option.correct && /*#__PURE__*/_react["default"].createElement("i", {
          style: {
            color: 'green'
          },
          className: "ml-3 fas fa-check-circle"
        }));
      }), error !== '' && /*#__PURE__*/_react["default"].createElement("label", {
        className: "mt-1 text-danger"
      }, error)));
    }
  }]);

  return RadioButtons;
}(_react["default"].Component);

var FilePicker = /*#__PURE__*/function (_React$Component9) {
  _inherits(FilePicker, _React$Component9);

  var _super9 = _createSuper(FilePicker);

  function FilePicker(props) {
    var _this11;

    _classCallCheck(this, FilePicker);

    _this11 = _super9.call(this, props);

    _defineProperty(_assertThisInitialized(_this11), "state", {
      uploadedFile: null
    });

    _this11.ref = /*#__PURE__*/_react["default"].createRef();
    return _this11;
  }

  _createClass(FilePicker, [{
    key: "render",
    value: function render() {
      var _this12 = this;

      var props = {};
      props.name = this.props.data.field_name;

      if (this.props.mutable) {
        props.ref = this.ref;
      }

      var baseClasses = "SortableItem rfb-item";
      var uploadedFile = this.state.uploadedFile;
      return /*#__PURE__*/_react["default"].createElement(_reactDropzone["default"], {
        onDrop: function onDrop(acceptedFiles) {
          acceptedFiles.forEach(function (file) {
            var reader = new FileReader();

            reader.onabort = function () {
              return console.log("file reading was aborted");
            };

            reader.onerror = function () {
              return console.log("file reading has failed");
            };

            reader.onload = function () {
              // Do whatever you want with the file contents
              var binaryStr = reader.result;

              _this12.setState({
                uploadedFile: file
              });
            };

            reader.readAsArrayBuffer(file);
          });
        }
      }, function (_ref) {
        var getRootProps = _ref.getRootProps,
            getInputProps = _ref.getInputProps;
        return /*#__PURE__*/_react["default"].createElement("div", {
          className: baseClasses
        }, /*#__PURE__*/_react["default"].createElement(ComponentHeader, _this12.props), /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-group"
        }, /*#__PURE__*/_react["default"].createElement(ComponentLabel, _this12.props), /*#__PURE__*/_react["default"].createElement("div", _extends({}, getRootProps(), {
          className: "react-dropzone",
          ref: _this12.ref
        }), /*#__PURE__*/_react["default"].createElement("input", getInputProps()), uploadedFile && /*#__PURE__*/_react["default"].createElement("p", null, "".concat(uploadedFile.name)), /*#__PURE__*/_react["default"].createElement("button", {
          className: "btn btn-primary m-8",
          onClick: function onClick(e) {
            e.preventDefault();
          }
        }, "Upload"))));
      }) // <div className={baseClasses}>
      //   <ComponentHeader {...props} />
      //   <div className="form-group">
      //     <div {...getRootProps()} className="d-flex" >
      //       <input {...getInputProps()} />
      //       <p>{`${uploadedFile ? uploadedFile.name : ''}`}</p>
      //       <button className="btn btn-primary m-8" onClick={(e) => { e.preventDefault(); }}>{'Upload'}</button>
      //     </div>
      //   </div>
      // </div>
      ;
    }
  }]);

  return FilePicker;
}(_react["default"].Component);

FormElements.TextInput = TextInput;
FormElements.NumberInput = NumberInput;
FormElements.TextArea = TextArea;
FormElements.Checkboxes = Checkboxes;
FormElements.DatePicker = DatePicker;
FormElements.TimePicker = DatePicker;
FormElements.RadioButtons = RadioButtons;
FormElements.FilePicker = FilePicker;
FormElements.Email = Email;
FormElements.Phone = Phone;
var _default = FormElements;
exports["default"] = _default;