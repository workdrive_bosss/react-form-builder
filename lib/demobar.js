"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _store = _interopRequireDefault(require("./stores/store"));

var _form = _interopRequireDefault(require("./form"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var answers = {}; // const answers = {
//   'dropdown_38716F53-51AA-4A53-9A9B-367603D82548': 'd2',
//   'checkboxes_8D6BDC45-76A3-4157-9D62-94B6B24BB833': [
//     'checkboxes_option_8657F4A6-AA5A-41E2-A44A-3E4F43BFC4A6',
//     'checkboxes_option_1D674F07-9E9F-4143-9D9C-D002B29BA9E4',
//   ],
//   'radio_buttons_F79ACC6B-7EBA-429E-870C-124F4F0DA90B': [
//     'radiobuttons_option_553B2710-AD7C-46B4-9F47-B2BD5942E0C7',
//   ],
//   'rating_3B3491B3-71AC-4A68-AB8C-A2B5009346CB': 4,
// };

var Demobar = /*#__PURE__*/function (_React$Component) {
  _inherits(Demobar, _React$Component);

  var _super = _createSuper(Demobar);

  function Demobar(props) {
    var _this;

    _classCallCheck(this, Demobar);

    _this = _super.call(this, props);
    _this.state = {
      data: [],
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false,
      noDateModalVisible: false
    };

    var update = _this._onChange.bind(_assertThisInitialized(_this));

    _this._onSubmit = _this._onSubmit.bind(_assertThisInitialized(_this));

    _store["default"].subscribe(function (state) {
      return update(state.data);
    });

    return _this;
  }

  _createClass(Demobar, [{
    key: "showPreview",
    value: function showPreview() {
      this.setState({
        previewVisible: true
      });
    }
  }, {
    key: "showShortPreview",
    value: function showShortPreview() {
      this.setState({
        shortPreviewVisible: true
      });
    }
  }, {
    key: "showRoPreview",
    value: function showRoPreview() {
      this.setState({
        roPreviewVisible: true
      });
    }
  }, {
    key: "showNoDataModal",
    value: function showNoDataModal() {
      this.setState({
        noDateModalVisible: true
      });
    }
  }, {
    key: "closePreview",
    value: function closePreview() {
      this.setState({
        previewVisible: false,
        shortPreviewVisible: false,
        roPreviewVisible: false,
        noDateModalVisible: false
      });
    }
  }, {
    key: "_onChange",
    value: function _onChange(data) {
      this.setState({
        data: data
      });
    } // eslint-disable-next-line no-unused-vars

  }, {
    key: "_onSubmit",
    value: function _onSubmit(data) {// console.log('onSubmit', data);
      // Place code to post json data to server here
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props$isQuiz = this.props.isQuiz,
          isQuiz = _this$props$isQuiz === void 0 ? false : _this$props$isQuiz;
      var modalClass = 'modal';

      if (this.state.previewVisible) {
        modalClass += ' show d-block';
      }

      var shortModalClass = 'modal short-modal';

      if (this.state.shortPreviewVisible) {
        shortModalClass += ' show d-block';
      }

      var roModalClass = 'modal ro-modal';

      if (this.state.roPreviewVisible) {
        roModalClass += ' show d-block';
      }

      return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("button", {
        className: "btn btn-primary",
        onClick: function onClick() {
          if (_this2.state.data.length > 0) {
            _this2.showPreview();
          } else {
            _this2.showNoDataModal();
          }
        }
      }, "".concat(!isQuiz ? 'Preview Form' : 'Preview Quiz')), this.state.previewVisible && /*#__PURE__*/_react["default"].createElement("div", {
        className: modalClass,
        role: "dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-dialog",
        role: "document"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-content"
      }, /*#__PURE__*/_react["default"].createElement("h3", null, this.props.title), /*#__PURE__*/_react["default"].createElement(_form["default"], {
        download_path: "",
        back_action: "/",
        back_name: "Back",
        closeModal: this.closePreview.bind(this),
        answer_data: answers,
        action_name: "Save",
        form_action: "/api/form",
        form_method: "POST",
        hide_actions: true // skip_validations={true}
        // onSubmit={this._onSubmit}
        ,
        variables: this.props.variables,
        data: this.state.data
      })))), this.state.noDateModalVisible && /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal",
        role: "dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-dialog",
        role: "document"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-content"
      }, /*#__PURE__*/_react["default"].createElement("h3", null, this.props.title), !isQuiz && /*#__PURE__*/_react["default"].createElement("label", null, "No Fields added to form yet."), isQuiz && /*#__PURE__*/_react["default"].createElement("label", null, "No Questions added to quiz yet."), /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-primary align-self-end",
        "data-dismiss": "modal",
        onClick: this.closePreview.bind(this)
      }, "Ok")))), this.state.roPreviewVisible && /*#__PURE__*/_react["default"].createElement("div", {
        className: roModalClass
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-content"
      }, /*#__PURE__*/_react["default"].createElement("h3", null, this.props.title), /*#__PURE__*/_react["default"].createElement(_form["default"], {
        download_path: "",
        back_action: "/",
        back_name: "Back",
        answer_data: answers,
        action_name: "Save",
        form_action: "/",
        form_method: "POST",
        read_only: true,
        variables: this.props.variables,
        hide_actions: true,
        data: this.state.data
      }), /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-footer"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-default",
        "data-dismiss": "modal",
        onClick: this.closePreview.bind(this)
      }, "Close"))))), this.state.shortPreviewVisible && /*#__PURE__*/_react["default"].createElement("div", {
        className: shortModalClass
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-dialog"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-content"
      }, /*#__PURE__*/_react["default"].createElement("h3", null, this.props.title), /*#__PURE__*/_react["default"].createElement(_form["default"], {
        download_path: "",
        back_action: "",
        answer_data: answers,
        form_action: "/",
        form_method: "POST",
        data: this.state.data,
        display_short: true,
        variables: this.props.variables,
        hide_actions: false
      }), /*#__PURE__*/_react["default"].createElement("div", {
        className: "modal-footer"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-default",
        "data-dismiss": "modal",
        onClick: this.closePreview.bind(this)
      }, "Close"))))));
    }
  }]);

  return Demobar;
}(_react["default"].Component);

exports["default"] = Demobar;