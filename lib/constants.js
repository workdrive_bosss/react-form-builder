"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertFromJson = exports.create = exports.quizQuestionItems = exports.formQuestionItems = exports.convertDataToJson = exports.fieldObject = exports._defaultItemOptions = exports.FORMIO_ITEMS_KEYS = exports.TOOLBOX_ITEMS_KEYS = void 0;

var _UUID = _interopRequireDefault(require("./UUID"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TOOLBOX_ITEMS_KEYS = {
  TEXTINPUT: 'TextInput',
  TEXTAREA: 'TextArea',
  CHECKBOXES: 'Checkboxes',
  RADIOBUTTONS: 'RadioButtons',
  NUMBERINPUT: 'NumberInput',
  DATEPICKER: 'DatePicker',
  TIMEPICKER: 'TimePicker',
  FILEPICKER: 'FilePicker',
  Email: 'Email',
  Phone: 'Phone'
};
exports.TOOLBOX_ITEMS_KEYS = TOOLBOX_ITEMS_KEYS;
var FORMIO_ITEMS_KEYS = {
  TEXTINPUT: 'textfield',
  TEXTAREA: 'textarea',
  CHECKBOXES: 'selectboxes',
  RADIOBUTTONS: 'radio',
  NUMBERINPUT: 'number',
  DATEPICKER: 'datetime',
  TIMEPICKER: 'time',
  FILEPICKER: 'file',
  Email: 'email',
  Phone: 'phone'
};
exports.FORMIO_ITEMS_KEYS = FORMIO_ITEMS_KEYS;

var _defaultItemOptions = function _defaultItemOptions(element) {
  switch (element) {
    case 'Dropdown':
      return [{
        value: 'place_holder_option_1',
        text: 'Place holder option 1',
        key: "dropdown_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'place_holder_option_2',
        text: 'Place holder option 2',
        key: "dropdown_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'place_holder_option_3',
        text: 'Place holder option 3',
        key: "dropdown_option_".concat(_UUID["default"].uuid())
      }];

    case 'Tags':
      return [{
        value: 'place_holder_tag_1',
        text: 'Place holder tag 1',
        key: "tags_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'place_holder_tag_2',
        text: 'Place holder tag 2',
        key: "tags_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'place_holder_tag_3',
        text: 'Place holder tag 3',
        key: "tags_option_".concat(_UUID["default"].uuid())
      }];

    case 'Checkboxes':
      return [{
        value: 'Option 1',
        text: 'Option 1',
        key: "checkboxes_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'Option 2',
        text: 'Option 2',
        key: "checkboxes_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'Option 3',
        text: 'Option 3',
        key: "checkboxes_option_".concat(_UUID["default"].uuid())
      }];

    case 'RadioButtons':
      return [{
        value: 'Option 1',
        text: 'Option 1',
        key: "radiobuttons_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'Option 2',
        text: 'Option 2',
        key: "radiobuttons_option_".concat(_UUID["default"].uuid())
      }, {
        value: 'Option 3',
        text: 'Option 3',
        key: "radiobuttons_option_".concat(_UUID["default"].uuid())
      }];

    default:
      return [];
  }
};

exports._defaultItemOptions = _defaultItemOptions;
var commonJsonProperties = {
  input: true,
  tableView: true,
  // inputType: "text",
  // label: "First Name",
  // key: "firstName",
  // placeholder: "Enter your first name",
  // prefix: '',
  // suffix: '',
  multiple: false,
  defaultValue: '',
  "protected": false,
  // unique: false,
  persistent: true,
  hidden: false,
  clearOnHide: true,
  // autofocus: true,
  labelPosition: 'top',
  tags: [],
  properties: {} // validate: {
  //   required: false,
  // },

};

var fieldObject = function fieldObject(item, index) {
  var element = item.element,
      label = item.label,
      required = item.required,
      _item$options = item.options,
      options = _item$options === void 0 ? [] : _item$options,
      timeFormat = item.timeFormat,
      dateFormat = item.dateFormat,
      _item$showTimeSelect = item.showTimeSelect,
      showTimeSelect = _item$showTimeSelect === void 0 ? false : _item$showTimeSelect,
      _item$placeholder = item.placeholder,
      placeholder = _item$placeholder === void 0 ? '' : _item$placeholder;

  var fieldProperties = _objectSpread(_objectSpread({}, commonJsonProperties), {}, {
    validate: {
      required: required
    },
    label: label
  });

  switch (element) {
    case TOOLBOX_ITEMS_KEYS.TEXTINPUT:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        inputType: 'text',
        key: "text".concat(index),
        type: 'textfield',
        spellcheck: true,
        placeholder: placeholder,
        inputMask: ''
      });
      break;

    case TOOLBOX_ITEMS_KEYS.Email:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        inputType: 'text',
        key: "email".concat(index),
        type: 'textfield',
        spellcheck: true,
        placeholder: placeholder,
        inputMask: ''
      });
      break;

    case TOOLBOX_ITEMS_KEYS.Phone:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        inputType: 'text',
        key: "phone".concat(index),
        type: 'textfield',
        spellcheck: true,
        placeholder: placeholder,
        inputMask: ''
      });
      break;

    case TOOLBOX_ITEMS_KEYS.TEXTAREA:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        key: "text".concat(index),
        type: 'textarea',
        placeholder: placeholder,
        spellcheck: true
      });
      break;

    case TOOLBOX_ITEMS_KEYS.NUMBERINPUT:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        key: "number".concat(index),
        inputType: 'number',
        placeholder: placeholder,
        type: 'number'
      });
      break;

    case TOOLBOX_ITEMS_KEYS.CHECKBOXES:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        inline: item.inline,
        type: 'selectboxes',
        key: "selectBoxes".concat(index),
        values: options.map(function (option) {
          return {
            label: option.value,
            value: option.value,
            shortcut: '',
            correct: option.correct
          };
        }),
        optionsLabelPosition: 'right'
      });
      break;

    case TOOLBOX_ITEMS_KEYS.RADIOBUTTONS:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        inline: item.inline,
        inputType: 'radio',
        type: 'radio',
        key: "radio".concat(index),
        optionsLabelPosition: 'right',
        values: options.map(function (option) {
          return {
            label: option.text,
            value: option.value,
            shortcut: '',
            correct: option.correct
          };
        })
      });
      break;

    case TOOLBOX_ITEMS_KEYS.DATEPICKER:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        autofocus: false,
        format: showTimeSelect ? "".concat(dateFormat, " ").concat(timeFormat) : dateFormat,
        enableDate: true,
        enableTime: showTimeSelect,
        inputType: 'time',
        type: 'datetime',
        key: "dateTime".concat(index),
        defaultDate: '',
        datepickerMode: 'day',
        datePicker: {
          showWeeks: true,
          startingDay: 0,
          initDate: '',
          minMode: 'day',
          maxMode: 'year',
          yearRows: 4,
          yearColumns: 5,
          minDate: null,
          maxDate: null,
          datepickerMode: 'day'
        }
      });

      if (showTimeSelect) {
        fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
          timePicker: {
            hourStep: 1,
            minuteStep: 1,
            showMeridian: true,
            readonlyInput: false,
            mousewheel: true,
            arrowkeys: true
          }
        });
      }

      break;

    case TOOLBOX_ITEMS_KEYS.TIMEPICKER:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        autofocus: false,
        format: timeFormat,
        inputType: 'time',
        type: 'time',
        key: "time".concat(index),
        inputFormat: 'plain'
      });
      break;

    case TOOLBOX_ITEMS_KEYS.FILEPICKER:
      fieldProperties = _objectSpread(_objectSpread({}, fieldProperties), {}, {
        filePattern: '*',
        fileMinSize: '0KB',
        fileMaxSize: '1GB',
        type: 'file',
        key: "file".concat(index),
        storage: 'url',
        labelPosition: 'top'
      }); // eslint-disable-next-line no-case-declarations

      var _fieldProperties = fieldProperties,
          suffix = _fieldProperties.suffix,
          prefix = _fieldProperties.prefix,
          spellcheck = _fieldProperties.spellcheck,
          inputMask = _fieldProperties.inputMask,
          unique = _fieldProperties.unique,
          rest = _objectWithoutProperties(_fieldProperties, ["suffix", "prefix", "spellcheck", "inputMask", "unique"]);

      fieldProperties = _objectSpread({}, rest);
      break;

    default:
      fieldProperties = {};
      break;
  }

  return fieldProperties;
};

exports.fieldObject = fieldObject;

var convertDataToJson = function convertDataToJson(data) {
  return JSON.stringify({
    data: {
      components: data.map(function (item, index) {
        return fieldObject(item, index + 1);
      }).concat({
        type: 'button',
        theme: 'primary',
        disableOnInvalid: true,
        action: 'submit',
        block: false,
        rightIcon: '',
        leftIcon: '',
        size: 'md',
        key: 'submit',
        tableView: false,
        label: 'Submit',
        input: true
      })
    }
  });
};

exports.convertDataToJson = convertDataToJson;
var textInputItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTINPUT,
  canHaveAnswer: true,
  name: 'Text Input',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.TEXTINPUT, " Editor"),
  label: 'Text Input Label',
  placeholder: 'Enter text',
  icon: 'cicon-text',
  field_name: 'text_input_'
};
var emailInputItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTINPUT,
  canHaveAnswer: true,
  name: 'Email',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.TEXTINPUT, " Editor"),
  label: 'Email',
  placeholder: 'Enter email',
  icon: 'cicon-email-outlined',
  field_name: 'email_'
};
var phoneInputItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTINPUT,
  canHaveAnswer: true,
  name: 'Phone',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.TEXTINPUT, " Editor"),
  label: 'Phone',
  placeholder: 'Enter Phone no.',
  icon: 'cicon-phone',
  field_name: 'phone_'
};
var textareaItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTAREA,
  canHaveAnswer: true,
  name: 'Text Area(Multiline Input)',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.TEXTAREA, " Editor"),
  label: 'Text area Label',
  placeholder: 'Enter paragraph',
  icon: 'cicon-text',
  field_name: 'text_area_'
};
var checkboxtem = {
  key: TOOLBOX_ITEMS_KEYS.CHECKBOXES,
  canHaveAnswer: true,
  name: 'Multiple choice',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.CHECKBOXES, " Editor"),
  label: 'Multiple Choice Question',
  icon: 'cicon-checkbox',
  field_name: 'checkboxes_',
  options: []
};
var radioButtonItem = {
  key: TOOLBOX_ITEMS_KEYS.RADIOBUTTONS,
  canHaveAnswer: true,
  name: 'Single Choice',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.RADIOBUTTONS, " Editor"),
  label: 'Single Choice Question',
  icon: 'cicon-radio-checked',
  field_name: 'radiobuttons_',
  options: []
};
var numberInputItem = {
  key: TOOLBOX_ITEMS_KEYS.NUMBERINPUT,
  canHaveAnswer: true,
  name: 'Numbers Input',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.NUMBERINPUT, " Editor"),
  label: 'Number Input Label',
  placeholder: 'Enter number input',
  icon: 'cicon-number-input',
  field_name: 'number_input_'
};
var datepickerItem = {
  key: TOOLBOX_ITEMS_KEYS.DATEPICKER,
  canDefaultToday: true,
  canReadOnly: true,
  dateFormat: 'MM/dd/yyyy',
  timeFormat: 'hh:mm a',
  showTimeSelect: false,
  showTimeSelectOnly: false,
  name: 'Date',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.DATEPICKER, " Editor"),
  label: 'Datepicker Label',
  icon: 'cicon-events',
  field_name: 'date_picker_'
};
var timepickerItem = {
  key: TOOLBOX_ITEMS_KEYS.TIMEPICKER,
  canDefaultToday: true,
  canReadOnly: true,
  dateFormat: 'MM/dd/yyyy',
  timeFormat: 'hh:mm a',
  showTimeSelect: true,
  showTimeSelectOnly: true,
  name: 'Time',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.TIMEPICKER, " Editor"),
  label: 'Timepicker Label',
  icon: 'cicon-time',
  field_name: 'date_picker_'
};
var filepickerItem = {
  key: TOOLBOX_ITEMS_KEYS.FILEPICKER,
  name: 'File Attachment',
  editorLabel: "".concat(TOOLBOX_ITEMS_KEYS.FILEPICKER, " Editor"),
  icon: 'cicon-attachment',
  label: 'FilePicker Label',
  field_name: 'file_picker_'
};
var formQuestionItems = [textInputItem, textareaItem, numberInputItem, checkboxtem, radioButtonItem, datepickerItem, emailInputItem, phoneInputItem // timepickerItem,
// filepickerItem,
];
exports.formQuestionItems = formQuestionItems;
var quizQuestionItems = [checkboxtem, radioButtonItem];
exports.quizQuestionItems = quizQuestionItems;

var create = function create(item) {
  var isQuiz = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var elementOptions = {
    id: _UUID["default"].uuid(),
    element: item.element || item.key,
    text: item.name || item.text,
    editorLabel: item.editorLabel,
    "static": item["static"],
    required: isQuiz
  };
  var itemKey = item.element || item.key;

  if (item.canHaveAnswer) {
    elementOptions.canHaveAnswer = item.canHaveAnswer;
  }

  if (item.filePicker) {
    elementOptions.filePicker = item.filePicker;
  }

  if (item.canReadOnly) {
    elementOptions.readOnly = false;
  }

  elementOptions.canHaveDisplayHorizontal = item.canHaveDisplayHorizontal !== false;
  elementOptions.canHaveOptionCorrect = item.canHaveOptionCorrect !== false;
  elementOptions.canHaveOptionValue = item.canHaveOptionValue !== false;
  elementOptions.canPopulateFromApi = item.canPopulateFromApi !== false;

  if (itemKey === TOOLBOX_ITEMS_KEYS.DATEPICKER || itemKey === TOOLBOX_ITEMS_KEYS.TIMEPICKER) {
    elementOptions.dateFormat = item.dateFormat;
    elementOptions.timeFormat = item.timeFormat;
    elementOptions.showTimeSelect = item.showTimeSelect;
    elementOptions.showTimeSelectOnly = item.showTimeSelectOnly;
  } // if (item.key === 'FilePicker') {
  //   elementOptions._href = item._href;
  //   elementOptions.file_path = item.file_path;
  // }


  if (item.defaultValue) {
    elementOptions.defaultValue = item.defaultValue;
  }

  if (item.field_name) {
    elementOptions.field_name = item.field_name + _UUID["default"].uuid();
  }

  if (item.label) {
    elementOptions.label = item.label;
  }

  if (item.placeholder) {
    elementOptions.placeholder = item.placeholder;
  }

  if (item.options) {
    if (item.options.length === 0) {
      elementOptions.options = _defaultItemOptions(elementOptions.element);
    } else {
      elementOptions.options = item.options;
    }
  }

  return elementOptions;
};

exports.create = create;

var getFormItem = function getFormItem(item) {
  var label = item.label,
      placeholder = item.placeholder,
      _item$validate = item.validate;
  _item$validate = _item$validate === void 0 ? {} : _item$validate;
  var _item$validate$requir = _item$validate.required,
      required = _item$validate$requir === void 0 ? false : _item$validate$requir,
      values = item.values,
      inline = item.inline;
  var commonProps = {
    label: label,
    required: required
  };

  switch (item.type) {
    case FORMIO_ITEMS_KEYS.TEXTINPUT:
    case FORMIO_ITEMS_KEYS.Email:
    case FORMIO_ITEMS_KEYS.Phone:
    case FORMIO_ITEMS_KEYS.TEXTAREA:
    case FORMIO_ITEMS_KEYS.NUMBERINPUT:
      return _objectSpread(_objectSpread({}, create(textInputItem)), {}, {
        placeholder: placeholder
      }, commonProps);

    case FORMIO_ITEMS_KEYS.RADIOBUTTONS:
      return _objectSpread(_objectSpread(_objectSpread({}, create(radioButtonItem)), commonProps), {}, {
        options: values.map(function (opt) {
          var option = {
            value: opt.value,
            text: opt.label,
            key: "radiobuttons_option_".concat(_UUID["default"].uuid())
          };

          if (opt.correct) {
            option.correct = true;
          }

          return option;
        }),
        inline: inline
      });

    case FORMIO_ITEMS_KEYS.CHECKBOXES:
      return _objectSpread(_objectSpread(_objectSpread({}, create(checkboxtem)), commonProps), {}, {
        options: values.map(function (opt) {
          var option = {
            value: opt.value,
            text: opt.label,
            key: "checkboxes_option_".concat(_UUID["default"].uuid())
          };

          if (opt.correct) {
            option.correct = true;
          }

          return option;
        }),
        inline: inline
      });

    case FORMIO_ITEMS_KEYS.DATEPICKER:
      return _objectSpread(_objectSpread({}, create(datepickerItem)), {}, {
        showTimeSelect: item.enableTime
      }, commonProps);

    case FORMIO_ITEMS_KEYS.TIMEPICKER:
      return _objectSpread(_objectSpread({}, create(timepickerItem)), commonProps);

    case FORMIO_ITEMS_KEYS.FILEPICKER:
      return _objectSpread(_objectSpread({}, create(filepickerItem)), commonProps);

    default:
      return null;
  }
};

var convertFromJson = function convertFromJson(data) {
  // const obj = JSON.parse(data);
  // console.log(data, obj)
  if (!data) {
    return [];
  }

  var items = data.data.components.map(function (comp) {
    return getFormItem(comp);
  });
  return items.filter(function (item) {
    if (item) {
      return item;
    }
  });
};

exports.convertFromJson = convertFromJson;