import ID from "./UUID";

export const TOOLBOX_ITEMS_KEYS = {
  TEXTINPUT: 'TextInput',
  TEXTAREA: 'TextArea',
  CHECKBOXES: 'Checkboxes',
  RADIOBUTTONS: 'RadioButtons',
  NUMBERINPUT: 'NumberInput',
  DATEPICKER: 'DatePicker',
  TIMEPICKER: 'TimePicker',
  FILEPICKER: 'FilePicker',
  Email: 'Email',
  Phone: 'Phone',
};

export const FORMIO_ITEMS_KEYS = {
  TEXTINPUT: 'textfield',
  TEXTAREA: 'textarea',
  CHECKBOXES: 'selectboxes',
  RADIOBUTTONS: 'radio',
  NUMBERINPUT: 'number',
  DATEPICKER: 'datetime',
  TIMEPICKER: 'time',
  FILEPICKER: 'file',
  Email: 'email',
  Phone: 'phone',
};

export const _defaultItemOptions = (element) => {
  switch (element) {
    case 'Dropdown':
      return [
        { value: 'place_holder_option_1', text: 'Place holder option 1', key: `dropdown_option_${ID.uuid()}` },
        { value: 'place_holder_option_2', text: 'Place holder option 2', key: `dropdown_option_${ID.uuid()}` },
        { value: 'place_holder_option_3', text: 'Place holder option 3', key: `dropdown_option_${ID.uuid()}` },
      ];
    case 'Tags':
      return [
        { value: 'place_holder_tag_1', text: 'Place holder tag 1', key: `tags_option_${ID.uuid()}` },
        { value: 'place_holder_tag_2', text: 'Place holder tag 2', key: `tags_option_${ID.uuid()}` },
        { value: 'place_holder_tag_3', text: 'Place holder tag 3', key: `tags_option_${ID.uuid()}` },
      ];
    case 'Checkboxes':
      return [
        { value: 'Option 1', text: 'Option 1', key: `checkboxes_option_${ID.uuid()}` },
        { value: 'Option 2', text: 'Option 2', key: `checkboxes_option_${ID.uuid()}` },
        { value: 'Option 3', text: 'Option 3', key: `checkboxes_option_${ID.uuid()}` },
      ];
    case 'RadioButtons':
      return [
        { value: 'Option 1', text: 'Option 1', key: `radiobuttons_option_${ID.uuid()}` },
        { value: 'Option 2', text: 'Option 2', key: `radiobuttons_option_${ID.uuid()}` },
        { value: 'Option 3', text: 'Option 3', key: `radiobuttons_option_${ID.uuid()}` },
      ];
    default:
      return [];
  }
}
const commonJsonProperties = {
  input: true,
  tableView: true,
  // inputType: "text",
  // label: "First Name",
  // key: "firstName",
  // placeholder: "Enter your first name",
  // prefix: '',
  // suffix: '',
  multiple: false,
  defaultValue: '',
  protected: false,
  // unique: false,
  persistent: true,
  hidden: false,
  clearOnHide: true,
  // autofocus: true,
  labelPosition: 'top',
  tags: [
  ],
  properties: {
  },
  // validate: {
  //   required: false,
  // },
};

export const fieldObject = (item, index) => {
  const {
    element, label, required, options = [], timeFormat, dateFormat, showTimeSelect = false, placeholder = ''
  } = item;
  let fieldProperties = {
    ...commonJsonProperties,
    validate: { required },
    label,
  };
  switch (element) {
    case TOOLBOX_ITEMS_KEYS.TEXTINPUT:
      fieldProperties = {
        ...fieldProperties,
        inputType: 'text',
        key: `text${index}`,
        type: 'textfield',
        spellcheck: true,
        placeholder,
        inputMask: '',
      };
      break;
    case TOOLBOX_ITEMS_KEYS.Email:
      fieldProperties = {
        ...fieldProperties,
        inputType: 'text',
        key: `email${index}`,
        type: 'textfield',
        spellcheck: true,
        placeholder,
        inputMask: '',
      };
      break;
    case TOOLBOX_ITEMS_KEYS.Phone:
      fieldProperties = {
        ...fieldProperties,
        inputType: 'text',
        key: `phone${index}`,
        type: 'textfield',
        spellcheck: true,
        placeholder,
        inputMask: '',
      };
      break;
    case TOOLBOX_ITEMS_KEYS.TEXTAREA:
      fieldProperties = {
        ...fieldProperties,
        key: `text${index}`,
        type: 'textarea',
        placeholder,
        spellcheck: true,
      };
      break;
    case TOOLBOX_ITEMS_KEYS.NUMBERINPUT:
      fieldProperties = {
        ...fieldProperties,
        key: `number${index}`,
        inputType: 'number',
        placeholder,
        type: 'number',
      };
      break;
    case TOOLBOX_ITEMS_KEYS.CHECKBOXES:
      fieldProperties = {
        ...fieldProperties,
        inline: item.inline,
        type: 'selectboxes',
        key: `selectBoxes${index}`,
        values: options.map((option) => ({
          label: option.value,
          value: option.value,
          shortcut: '',
          correct: option.correct,
        })),
        optionsLabelPosition: 'right',
      };
      break;
    case TOOLBOX_ITEMS_KEYS.RADIOBUTTONS:
      fieldProperties = {
        ...fieldProperties,
        inline: item.inline,
        inputType: 'radio',
        type: 'radio',
        key: `radio${index}`,
        optionsLabelPosition: 'right',
        values: options.map((option) => ({
          label: option.text,
          value: option.value,
          shortcut: '',
          correct: option.correct,
        })),
      };
      break;
    case TOOLBOX_ITEMS_KEYS.DATEPICKER:
      fieldProperties = {
        ...fieldProperties,
        autofocus: false,
        format: showTimeSelect ? `${dateFormat} ${timeFormat}` : dateFormat,
        enableDate: true,
        enableTime: showTimeSelect,
        inputType: 'time',
        type: 'datetime',
        key: `dateTime${index}`,
        defaultDate: '',
        datepickerMode: 'day',
        datePicker: {
          showWeeks: true,
          startingDay: 0,
          initDate: '',
          minMode: 'day',
          maxMode: 'year',
          yearRows: 4,
          yearColumns: 5,
          minDate: null,
          maxDate: null,
          datepickerMode: 'day',
        },
      };
      if (showTimeSelect) {
        fieldProperties = {
          ...fieldProperties,
          timePicker: {
            hourStep: 1,
            minuteStep: 1,
            showMeridian: true,
            readonlyInput: false,
            mousewheel: true,
            arrowkeys: true,
          },
        };
      }
      break;
    case TOOLBOX_ITEMS_KEYS.TIMEPICKER:
      fieldProperties = {
        ...fieldProperties,
        autofocus: false,
        format: timeFormat,
        inputType: 'time',
        type: 'time',
        key: `time${index}`,
        inputFormat: 'plain',
      };
      break;
    case TOOLBOX_ITEMS_KEYS.FILEPICKER:
      fieldProperties = {
        ...fieldProperties,
        filePattern: '*',
        fileMinSize: '0KB',
        fileMaxSize: '1GB',
        type: 'file',
        key: `file${index}`,
        storage: 'url',
        labelPosition: 'top',
      };
      // eslint-disable-next-line no-case-declarations
      const {
        suffix, prefix, spellcheck, inputMask, unique, ...rest
      } = fieldProperties;
      fieldProperties = { ...rest };
      break;
    default:

      fieldProperties = {};
      break;
  }
  return fieldProperties;
};

export const convertDataToJson = (data) => JSON.stringify({
  data: {
    components: data.map((item, index) => fieldObject(item, index + 1)).concat({
      type: 'button',
      theme: 'primary',
      disableOnInvalid: true,
      action: 'submit',
      block: false,
      rightIcon: '',
      leftIcon: '',
      size: 'md',
      key: 'submit',
      tableView: false,
      label: 'Submit',
      input: true,
    })
  }
});

const textInputItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTINPUT,
  canHaveAnswer: true,
  name: 'Text Input',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.TEXTINPUT} Editor`,
  label: 'Text Input Label',
  placeholder: 'Enter text',
  icon: 'cicon-text',
  field_name: 'text_input_',
};
const emailInputItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTINPUT,
  canHaveAnswer: true,
  name: 'Email',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.TEXTINPUT} Editor`,
  label: 'Email',
  placeholder: 'Enter email',
  icon: 'cicon-email-outlined',
  field_name: 'email_',
};
const phoneInputItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTINPUT,
  canHaveAnswer: true,
  name: 'Phone',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.TEXTINPUT} Editor`,
  label: 'Phone',
  placeholder: 'Enter Phone no.',
  icon: 'cicon-phone',
  field_name: 'phone_',
};
const textareaItem = {
  key: TOOLBOX_ITEMS_KEYS.TEXTAREA,
  canHaveAnswer: true,
  name: 'Text Area(Multiline Input)',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.TEXTAREA} Editor`,
  label: 'Text area Label',
  placeholder: 'Enter paragraph',
  icon: 'cicon-text',
  field_name: 'text_area_',
};

const checkboxtem = {
  key: TOOLBOX_ITEMS_KEYS.CHECKBOXES,
  canHaveAnswer: true,
  name: 'Multiple choice',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.CHECKBOXES} Editor`,
  label: 'Multiple Choice Question',
  icon: 'cicon-checkbox',
  field_name: 'checkboxes_',
  options: [],
};

const radioButtonItem = {
  key: TOOLBOX_ITEMS_KEYS.RADIOBUTTONS,
  canHaveAnswer: true,
  name: 'Single Choice',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.RADIOBUTTONS} Editor`,
  label: 'Single Choice Question',
  icon: 'cicon-radio-checked',
  field_name: 'radiobuttons_',
  options: [],
};

const numberInputItem = {
  key: TOOLBOX_ITEMS_KEYS.NUMBERINPUT,
  canHaveAnswer: true,
  name: 'Numbers Input',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.NUMBERINPUT} Editor`,
  label: 'Number Input Label',
  placeholder: 'Enter number input',
  icon: 'cicon-number-input',
  field_name: 'number_input_',
};

const datepickerItem = {
  key: TOOLBOX_ITEMS_KEYS.DATEPICKER,
  canDefaultToday: true,
  canReadOnly: true,
  dateFormat: 'MM/dd/yyyy',
  timeFormat: 'hh:mm a',
  showTimeSelect: false,
  showTimeSelectOnly: false,
  name: 'Date',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.DATEPICKER} Editor`,
  label: 'Datepicker Label',
  icon: 'cicon-events',
  field_name: 'date_picker_',
};

const timepickerItem = {
  key: TOOLBOX_ITEMS_KEYS.TIMEPICKER,
  canDefaultToday: true,
  canReadOnly: true,
  dateFormat: 'MM/dd/yyyy',
  timeFormat: 'hh:mm a',
  showTimeSelect: true,
  showTimeSelectOnly: true,
  name: 'Time',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.TIMEPICKER} Editor`,
  label: 'Timepicker Label',
  icon: 'cicon-time',
  field_name: 'date_picker_',
};

const filepickerItem = {
  key: TOOLBOX_ITEMS_KEYS.FILEPICKER,
  name: 'File Attachment',
  editorLabel: `${TOOLBOX_ITEMS_KEYS.FILEPICKER} Editor`,
  icon: 'cicon-attachment',
  label: 'FilePicker Label',
  field_name: 'file_picker_',
};

export const formQuestionItems = [
  textInputItem,
  textareaItem,
  numberInputItem,
  checkboxtem,
  radioButtonItem,
  datepickerItem,
  emailInputItem,
  phoneInputItem
  // timepickerItem,
  // filepickerItem,
];


export const quizQuestionItems = [
  checkboxtem,
  radioButtonItem,
];

export const create = (item, isQuiz = false) => {
  const elementOptions = {
    id: ID.uuid(),
    element: item.element || item.key,
    text: item.name || item.text,
    editorLabel: item.editorLabel,
    static: item.static,
    required: isQuiz,
  };

  const itemKey = item.element || item.key
  if (item.canHaveAnswer) { elementOptions.canHaveAnswer = item.canHaveAnswer; }
  if (item.filePicker) { elementOptions.filePicker = item.filePicker; }

  if (item.canReadOnly) { elementOptions.readOnly = false; }

  elementOptions.canHaveDisplayHorizontal = item.canHaveDisplayHorizontal !== false;
  elementOptions.canHaveOptionCorrect = item.canHaveOptionCorrect !== false;
  elementOptions.canHaveOptionValue = item.canHaveOptionValue !== false;
  elementOptions.canPopulateFromApi = item.canPopulateFromApi !== false;

  if (itemKey === TOOLBOX_ITEMS_KEYS.DATEPICKER || itemKey === TOOLBOX_ITEMS_KEYS.TIMEPICKER) {
    elementOptions.dateFormat = item.dateFormat;
    elementOptions.timeFormat = item.timeFormat;
    elementOptions.showTimeSelect = item.showTimeSelect;
    elementOptions.showTimeSelectOnly = item.showTimeSelectOnly;
  }

  // if (item.key === 'FilePicker') {
  //   elementOptions._href = item._href;
  //   elementOptions.file_path = item.file_path;
  // }

  if (item.defaultValue) { elementOptions.defaultValue = item.defaultValue; }

  if (item.field_name) { elementOptions.field_name = item.field_name + ID.uuid(); }

  if (item.label) { elementOptions.label = item.label; }
  if (item.placeholder) { elementOptions.placeholder = item.placeholder; }

  if (item.options) {
    if (item.options.length === 0) {
      elementOptions.options = _defaultItemOptions(elementOptions.element);
    } else {
      elementOptions.options = item.options;
    }
  }

  return elementOptions;
}
const getFormItem = (item) => {

  const { label, placeholder, validate: { required = false } = {}, values, inline } = item
  const commonProps = {
    label,
    required,
  }
  switch (item.type) {
    case FORMIO_ITEMS_KEYS.TEXTINPUT:
    case FORMIO_ITEMS_KEYS.Email:
    case FORMIO_ITEMS_KEYS.Phone:
    case FORMIO_ITEMS_KEYS.TEXTAREA:
    case FORMIO_ITEMS_KEYS.NUMBERINPUT:
      return {
        ...create(textInputItem),
        placeholder,
        ...commonProps,
      }

    case FORMIO_ITEMS_KEYS.RADIOBUTTONS:
      return {
        ...create(radioButtonItem),
        ...commonProps,
        options: values.map((opt) => {
          const option = { value: opt.value, text: opt.label, key: `radiobuttons_option_${ID.uuid()}` };
          if (opt.correct) {
            option.correct = true;
          }
          return option;
        }),
        inline,
      }
    case FORMIO_ITEMS_KEYS.CHECKBOXES:
      return {
        ...create(checkboxtem),
        ...commonProps,
        options: values.map((opt) => {
          const option = { value: opt.value, text: opt.label, key: `checkboxes_option_${ID.uuid()}` };
          if (opt.correct) {
            option.correct = true;
          }
          return option;
        }),
        inline,
      }
    case FORMIO_ITEMS_KEYS.DATEPICKER:
      return {
        ...create(datepickerItem),
        showTimeSelect: item.enableTime,
        ...commonProps,
      }
    case FORMIO_ITEMS_KEYS.TIMEPICKER:
      return {
        ...create(timepickerItem),
        ...commonProps,
      }
    case FORMIO_ITEMS_KEYS.FILEPICKER:
      return {
        ...create(filepickerItem),
        ...commonProps,
      }
    default:
      return null;
  }
};

export const convertFromJson = (data) => {
  // const obj = JSON.parse(data);
  // console.log(data, obj)
  if (!data) {
    return [];
  }
  const items = data.data.components.map((comp) => {
    return getFormItem(comp);
  });

  return items.filter((item) => { if (item) { return item } })
};