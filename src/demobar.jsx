import React from 'react';
import store from './stores/store';
import ReactFormGenerator from './form';

const answers = {};
// const answers = {
//   'dropdown_38716F53-51AA-4A53-9A9B-367603D82548': 'd2',
//   'checkboxes_8D6BDC45-76A3-4157-9D62-94B6B24BB833': [
//     'checkboxes_option_8657F4A6-AA5A-41E2-A44A-3E4F43BFC4A6',
//     'checkboxes_option_1D674F07-9E9F-4143-9D9C-D002B29BA9E4',
//   ],
//   'radio_buttons_F79ACC6B-7EBA-429E-870C-124F4F0DA90B': [
//     'radiobuttons_option_553B2710-AD7C-46B4-9F47-B2BD5942E0C7',
//   ],
//   'rating_3B3491B3-71AC-4A68-AB8C-A2B5009346CB': 4,
// };

export default class Demobar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false,
      noDateModalVisible: false,
    };

    const update = this._onChange.bind(this);
    this._onSubmit = this._onSubmit.bind(this);

    store.subscribe(state => update(state.data));
  }

  showPreview() {
    this.setState({
      previewVisible: true,
    });
  }

  showShortPreview() {
    this.setState({
      shortPreviewVisible: true,
    });
  }

  showRoPreview() {
    this.setState({
      roPreviewVisible: true,
    });
  }

  showNoDataModal() {
    this.setState({
      noDateModalVisible: true,
    });
  }

  closePreview() {
    this.setState({
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false,
      noDateModalVisible: false,
    });
  }

  _onChange(data) {
    this.setState({
      data,
    });
  }

  // eslint-disable-next-line no-unused-vars
  _onSubmit(data) {
    // console.log('onSubmit', data);
    // Place code to post json data to server here
  }

  render() {
    const { isQuiz = false } = this.props
    let modalClass = 'modal';
    if (this.state.previewVisible) {
      modalClass += ' show d-block';
    }
    let shortModalClass = 'modal short-modal';
    if (this.state.shortPreviewVisible) {
      shortModalClass += ' show d-block';
    }
    let roModalClass = 'modal ro-modal';
    if (this.state.roPreviewVisible) {
      roModalClass += ' show d-block';
    }
    return <>
      {/* <div className=""> */}
        {/* <h4 style={{ marginBottom: 0 }}>Preview</h4> */}
        {/* <span className="btn btn-default" style={{ marginRight: '10px', padding: 0 }} onClick={() => this.showRoPreview()}>Read Only Form</span>
          <span className="btn btn-default" style={{ marginRight: '10px', padding: 0 }} onClick={() => this.showShortPreview()}>Alternate/Short Form</span> */}
        <button className="btn btn-primary" onClick={() => {
          if (this.state.data.length > 0) {
            this.showPreview();
          } else {
            this.showNoDataModal();
          }
      }}>{`${!isQuiz ? 'Preview Form' : 'Preview Quiz'}`}</button>
      {/* </div> */}
      {this.state.previewVisible &&
        <div className={modalClass} role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
            <h3>{this.props.title}</h3>
              <ReactFormGenerator
                download_path=""
                back_action="/"
                back_name="Back"
                closeModal={this.closePreview.bind(this)}
                answer_data={answers}
                action_name="Save"
                form_action="/api/form"
                form_method="POST"
                hide_actions={true}
                // skip_validations={true}
                // onSubmit={this._onSubmit}
                variables={this.props.variables}
                data={this.state.data} />

              {/* <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.closePreview.bind(this)}>Close</button>
                </div> */}
            </div>
          </div>
        </div>
      }
      {
        this.state.noDateModalVisible && <div className="modal" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <h3>{this.props.title}</h3>
              {!isQuiz && <label>No Fields added to form yet.</label>}
              {isQuiz && <label>No Questions added to quiz yet.</label>}
              <button type="button" className="btn btn-primary align-self-end" data-dismiss="modal" onClick={this.closePreview.bind(this)}>Ok</button>
            </div>
          </div>
        </div>
      }

      {this.state.roPreviewVisible &&
        <div className={roModalClass}>
          <div className="modal-dialog">
            <div className="modal-content">
            <h3>{this.props.title}</h3>
              <ReactFormGenerator
                download_path=""
                back_action="/"
                back_name="Back"
                answer_data={answers}
                action_name="Save"
                form_action="/"
                form_method="POST"
                read_only={true}
                variables={this.props.variables}
                hide_actions={true}
                data={this.state.data} />

              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.closePreview.bind(this)}>Close</button>
              </div>
            </div>
          </div>
        </div>
      }

      {this.state.shortPreviewVisible &&
        <div className={shortModalClass}>
          <div className="modal-dialog">
            <div className="modal-content">
            <h3>{this.props.title}</h3>
              <ReactFormGenerator
                download_path=""
                back_action=""
                answer_data={answers}
                form_action="/"
                form_method="POST"
                data={this.state.data}
                display_short={true}
                variables={this.props.variables}
                hide_actions={false} />

              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.closePreview.bind(this)}>Close</button>
              </div>
            </div>
          </div>
        </div>
      }
    </>;
  }
}
