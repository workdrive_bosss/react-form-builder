/**
  * <ReactFormBuilder />
*/

import React from 'react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Preview from './preview';
import Toolbar from './toolbar';
import ReactFormGenerator from './form';
import store from './stores/store';
import { convertFromJson, formQuestionItems, quizQuestionItems } from './constants';

class ReactFormBuilder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      editElement: null,
    };

  }

  editModeOn(data, e) {
    e.preventDefault();
    e.stopPropagation();
    if (this.state.editMode) {
      this.setState({ editMode: !this.state.editMode, editElement: null });
    } else {
      this.setState({ editMode: !this.state.editMode, editElement: data });
    }
  }

  manualEditModeOff() {
    if (this.state.editMode) {
      this.setState({
        editMode: false,
        editElement: null,
      });
    }
  }
  scrollToBottom = () => {
    const { scrollAbleParentOffset = 0 } = this.props
    let root = document.getElementById('form-builder-root')
    for (let i = 0; i < scrollAbleParentOffset; ++i) {
      if (root.parentNode) {
        root = root.parentNode
      }
    }
    if (root) {
      setTimeout(() => {
        root.scrollTo({ top: root.clientHeight + 150, behavior: 'smooth' })
      }, 300)
    }
  }
  _renderPreview = () => (<Preview files={[{ id: 1, file_name: 'file_a' }]}
    manualEditModeOff={this.manualEditModeOff.bind(this)}
    showCorrectColumn={this.props.showCorrectColumn}
    parent={this}
    data={this.props.data}
    initialData={convertFromJson(this.props.initialData)}
    onSaveForm={this.props.onSaveForm}
    onCancelForm={this.props.onCancelForm}
    url={this.props.url}
    saveUrl={this.props.saveUrl}
    onLoad={this.props.onLoad}
    onPost={this.props.onPost}
    editModeOn={this.editModeOn}
    editMode={this.state.editMode}
    variables={this.props.variables}
    editElement={this.state.editElement}
    isQuiz={this.props.isQuiz}
    isReadOnly={this.props.isReadOnly}
    loading={this.props.loading}
    title={this.props.title}
    isRequiredMessage={this.props.isRequiredMessage}
  />)
  render() {
    const { isReadOnly = false } = this.props
    const toolbarProps = {};
    let style = { display: 'flex', width: '100%' }
    if (isReadOnly) {
      style = Object.assign({ flex: 1, overflowY: 'auto', overflowX: 'visible', padding: 5 })
    }
    if (this.props.toolbarItems) { toolbarProps.items = this.props.toolbarItems; }
    return (
      <DndProvider backend={HTML5Backend}>
        <div style={style} id="form-builder-root">
          {/* <div>
           <p>
             It is easy to implement a sortable interface with React DnD. Just make
             the same component both a drag source and a drop target, and reorder
             the data in the <code>hover</code> handler.
           </p>
           <Container />
         </div> */}
          <div className={`react-form-builder clearfix ${this.props.isQuiz ? 'quiz-form' : ''} ${isReadOnly ? 'read-only' : ''}`}>
            <div style={{
              display: 'flex',
              flex: 1,
              justifyContent: 'space-between',
            }}>
              <div className="d-flex flex-column w-75">
                {this._renderPreview()}
              </div>
              {!isReadOnly && <Toolbar
                isQuiz={this.props.isQuiz}
                items={this.props.isQuiz ? quizQuestionItems : formQuestionItems}
                scrollToBottom={this.scrollToBottom}
                {...toolbarProps}
              />}
            </div>
          </div>
        </div>
      </DndProvider >
    );
  }
}

const FormBuilders = {};
FormBuilders.ReactFormBuilder = ReactFormBuilder;
FormBuilders.ReactFormGenerator = ReactFormGenerator;
FormBuilders.ElementStore = store;

export default FormBuilders;

export { ReactFormBuilder, ReactFormGenerator, store as ElementStore };
