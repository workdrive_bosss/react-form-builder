/**
  * <Toolbar />
  */

import React from 'react';
import ToolbarItem from './toolbar-draggable-item';
import ID from './UUID';
import store from './stores/store';
import { TOOLBOX_ITEMS_KEYS, formQuestionItems, _defaultItemOptions, create } from './constants';
import * as variables from '../variables';
import Demobar from './demobar';

export default class Toolbar extends React.Component {
  constructor(props) {
    super(props);

    const items = (this.props.items) ? this.props.items : formQuestionItems;
    this.state = {
      items,
    };
    store.subscribe(state => this.setState({ store: state }));
  }

  // create(item) {
  //   const elementOptions = {
  //     id: ID.uuid(),
  //     element: item.element || item.key,
  //     text: item.name,
  //     editorLabel: item.editorLabel,
  //     static: item.static,
  //     required: false,
  //   };

  //   if (item.static) {
  //     elementOptions.bold = false;
  //     elementOptions.italic = false;
  //   }

  //   if (item.canHaveAnswer) { elementOptions.canHaveAnswer = item.canHaveAnswer; }
  //   if (item.filePicker) { elementOptions.filePicker = item.filePicker; }

  //   if (item.canReadOnly) { elementOptions.readOnly = false; }

  //   if (item.canDefaultToday) { elementOptions.defaultToday = false; }

  //   if (item.content) { elementOptions.content = item.content; }

  //   if (item.href) { elementOptions.href = item.href; }
  //   elementOptions.canHaveDisplayHorizontal = item.canHaveDisplayHorizontal !== false;
  //   elementOptions.canHaveOptionCorrect = item.canHaveOptionCorrect !== false;
  //   elementOptions.canHaveOptionValue = item.canHaveOptionValue !== false;
  //   elementOptions.canPopulateFromApi = item.canPopulateFromApi !== false;

  //   if (item.key === TOOLBOX_ITEMS_KEYS.DATEPICKER || item.key === TOOLBOX_ITEMS_KEYS.TIMEPICKER) {
  //     elementOptions.dateFormat = item.dateFormat;
  //     elementOptions.timeFormat = item.timeFormat;
  //     elementOptions.showTimeSelect = item.showTimeSelect;
  //     elementOptions.showTimeSelectOnly = item.showTimeSelectOnly;
  //   }

  //   // if (item.key === 'FilePicker') {
  //   //   elementOptions._href = item._href;
  //   //   elementOptions.file_path = item.file_path;
  //   // }

  //   if (item.defaultValue) { elementOptions.defaultValue = item.defaultValue; }

  //   if (item.field_name) { elementOptions.field_name = item.field_name + ID.uuid(); }

  //   if (item.label) { elementOptions.label = item.label; }
  //   if (item.placeholder) { elementOptions.placeholder = item.placeholder; }

  //   if (item.options) {
  //     elementOptions.options = _defaultItemOptions(elementOptions.element);
  //   }

  //   console.log(elementOptions);
  //   return elementOptions;
  // }

  _onClick(item) {
    // ElementActions.createElement(this.create(item));
    const { isQuiz = false, scrollToBottom = () => { } } = this.props
    scrollToBottom()
    store.dispatch('create', create(item, isQuiz));
  }

  render() {
    const { isQuiz = false } = this.props;
    return (
      <div className="react-form-builder-toolbar">
        <h4>{`${isQuiz ? 'Add new question' : 'Add new option '}`}</h4>
        <ul>
          {
            this.state.items.map((item) => (<ToolbarItem data={item} key={item.key} onClick={this._onClick.bind(this, item)} onCreate={this.create} />))
          }
        </ul>
        {/* <Demobar variables={variables} /> */}
      </div>
    );
  }
}
