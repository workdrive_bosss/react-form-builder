/**
 * <Preview />
 */

import React from "react";
import update from "immutability-helper";
import store from "./stores/store";
import FormElementsEdit from "./form-elements-edit";
import SortableFormElements from "./sortable-form-elements";
import { convertDataToJson, create } from "./constants";
import * as variables from "../variables";

import Demobar from "./demobar";

const { PlaceHolder } = SortableFormElements;

export default class Preview extends React.Component {
  constructor(props) {
    super(props);

    const { onLoad, onPost, initialData = [] } = props;
    store.setExternalHandler(onLoad, onPost);

    this.editForm = React.createRef();
    if (initialData && initialData.length > 0) {
      initialData.forEach((item) => {
        store.dispatch("create", create(item));
      });
    }
    this.state = {
      data: initialData,
      answer_data: {},
      showCancelModal: false,
      showModal: false,
      deleteItem: null,
      invalidForm: false
    };
    this.seq = 0;

    const onUpdate = this._onChange.bind(this);
    store.subscribe((state) => onUpdate(state.data));

    this.moveCard = this.moveCard.bind(this);
    this.insertCard = this.insertCard.bind(this);
    this.cancelForm = this.cancelForm.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // console.log("newProps", nextProps);
    if (this.props.data !== nextProps.data) {
      store.dispatch("updateOrder", nextProps.data);
    }
    // if (this.props.initialData !== nextProps.initialData) {
    //   console.log('newProps-inital', nextProps)

    //   nextProps.initialData.forEach((item) => {
    //     store.dispatch('create', create(item));
    //   });
    //   this.setState = ({
    //     data: nextProps.initialData,
    //   });
    // }
  }

  componentDidMount() {
    const { data, url, saveUrl, initialData } = this.props;
    store.dispatch("load", { loadUrl: url, saveUrl, data: initialData || [] });
    document.addEventListener("mousedown", this.editModeOff);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.editModeOff);
  }

  editModeOff = (e) => {
    if (this.editForm.current && !this.editForm.current.contains(e.target)) {
      this.manualEditModeOff();
    }
  };

  manualEditModeOff = () => {
    const { editElement } = this.props;
    if (editElement && editElement.dirty) {
      editElement.dirty = false;
    }
    this.props.manualEditModeOff();
  };

  _setValue(text) {
    // return text.replace(/[^A-Z0-9]+/gi, "_").toLowerCase();
    return text;
  }

  updateElement(element) {
    const { data } = this.state;
    let found = false;
    for (let i = 0, len = data.length; i < len; i++) {
      if (element.id === data[i].id) {
        data[i] = element;
        found = true;
        break;
      }
    }

    if (found) {
      this.seq = this.seq > 100000 ? 0 : this.seq + 1;
      store.dispatch("updateOrder", data);
    }
  }

  _onChange(data) {
    const answer_data = {};

    data.forEach((item) => {
      if (item && item.readOnly && this.props.variables[item.variableKey]) {
        answer_data[item.field_name] = this.props.variables[item.variableKey];
      }
    });

    this.setState({
      data,
      answer_data,
    });
  }

  _toggleModalVisibility(item) {
    this.setState({ showModal: !this.state.showModal, deleteItem: item });
  }

  cancelForm() {
    const { data = [] } = this.state;
    if (data.length > 0) {
      return this._toggleCancelModalVisibility();
    }
    if (!this.props.onCancelForm) {
      return;
    }
    this.props.onCancelForm();
  }

  _toggleCancelModalVisibility() {
    this.setState({ showCancelModal: !this.state.showCancelModal });
  }

  _onDestroy() {
    this._toggleModalVisibility();
    store.dispatch("delete", this.state.deleteItem);
  }

  insertCard(item, hoverIndex) {
    const { data } = this.state;
    const { isReadOnly = false } = this.props
    if (isReadOnly) {
      return
    }
    data.splice(hoverIndex, 0, item);
    this.saveData(item, hoverIndex, hoverIndex);
  }

  moveCard(dragIndex, hoverIndex) {
    const { isReadOnly = false } = this.props
    if (isReadOnly) {
      return
    }
    const { data } = this.state;
    const dragCard = data[dragIndex];
    this.saveData(dragCard, dragIndex, hoverIndex);
  }

  // eslint-disable-next-line no-unused-vars
  cardPlaceHolder(dragIndex, hoverIndex) {
    // Dummy
  }

  saveData(dragCard, dragIndex, hoverIndex) {
    const newData = update(this.state, {
      data: {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragCard],
        ],
      },
    });
    this.setState(newData);
    store.dispatch("updateOrder", newData.data);
  }

  getElement(item, index) {
    const SortableFormElement = SortableFormElements[item.element];
    return (
      <SortableFormElement
        id={item.id}
        seq={this.seq}
        index={index}
        moveCard={this.moveCard}
        insertCard={this.insertCard}
        mutable={false}
        parent={this.props.parent}
        editModeOn={this.props.editModeOn}
        isDraggable={false}
        canDrag={false}
        key={item.id}
        sortData={item.id}
        data={item}
        _onDestroy={this._toggleModalVisibility.bind(this)}
        isQuiz={this.props.isQuiz}
        isReadOnly={this.props.isReadOnly}
      />
    );
  }

  validateForm = () => {
    let valid = true;
    const { data = [] } = this.state;
    if (data.length === 0) {
      return false
    }
    data.forEach(currentQuestion => {
      const { options = [] } = currentQuestion;
      let isCorrect = false;
      options.forEach(currentOption => {
        if (currentOption.correct) {
          isCorrect = true;
        }
      });
      if (!isCorrect) {
        valid = false;
      }
    });
    return valid;
  }

  onSaveHandler() {
    const { isQuiz = false, loading = false } = this.props;
    if (loading) {
      return
    }
    if (isQuiz) {
      const isValidForm = this.validateForm();
      if (!isValidForm) {
        this.setState({ invalidForm: true })
        return;
      }
      this.setState({ invalidForm: true })
    }
    if (!this.props.onSaveForm) {
      return;
    }
    this.props.onSaveForm(convertDataToJson(this.state.data));
  }

  onCancelHandler() {
    this._toggleCancelModalVisibility();
    if (!this.props.onCancelForm) {
      return;
    }
    this.props.onCancelForm();
  }

  render() {
    let classes = this.props.className;
    const { showModal, deleteItem, showCancelModal } = this.state;
    const { isReadOnly = false, loading = false, isRequiredMessage = "" } = this.props
    if (this.props.editMode) {
      classes += " is-editing";
    }
    const data = this.state.data.filter((x) => !!x);
    const items = data.map((item, index) => this.getElement(item, index));

    return (
      <div className="preview-wrapper">
        <div className={classes}>
          {showModal && (
            <div id="deleteModal" className="modal show d-block" role="dialog">
              <div className="modal-dialog">
                <div className="modal-content d-flex flex-col">
                  <label className="modal-title">{`Are you sure you want to delete field "${deleteItem.label}"?`}</label>
                  <span>This action cannot be undone</span>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      onClick={this._onDestroy.bind(this)}
                    >
                      Delete
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      data-dismiss="modal"
                      onClick={() => this._toggleModalVisibility(null)}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
          {showCancelModal && data.length > 0 && (
            <div id="deleteModal" className="modal show d-block" role="dialog">
              <div className="modal-dialog">
                <div className="modal-content d-flex flex-col">
                  <label className="modal-title">
                    {"Do you want to discard the changes?"}
                  </label>
                  <span>This action cannot be undone</span>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      onClick={this.onCancelHandler.bind(this)}
                    >
                      Discard
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      data-dismiss="modal"
                      onClick={() => this._toggleCancelModalVisibility()}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
          <div ref={this.editForm}>
            {this.props.editElement !== null && (
              <FormElementsEdit
                showCorrectColumn={this.props.showCorrectColumn}
                files={this.props.files}
                manualEditModeOff={this.manualEditModeOff}
                preview={this}
                element={this.props.editElement}
                updateElement={this.updateElement}
                isQuiz={this.props.isQuiz}
              />
            )}
          </div>
          {items.length > 0 && <div className="Sortable">{items}</div>}
          {items.length === 0 && (
            <PlaceHolder
              id="form-place-holder"
              show={items.length === 0}
              index={items.length}
              text={
                this.props.isQuiz
                  ? "Add question from right menu"
                  : "Add form option from right menu"
              }
              moveCard={this.cardPlaceHolder}
              insertCard={this.insertCard}
            />
          )}
          {!!isRequiredMessage &&
            this.state.invalidForm &&
            items.length === 0 && (
              <div color="#a4262c" style={{ color: "#a4262c" }} className="mt-5">{isRequiredMessage}</div>
            )}
        </div>
        {!isReadOnly && (
          <div className="bottomBtnBar">
            <div className="btn-container">
              <Demobar
                variables={variables}
                isQuiz={this.props.isQuiz}
                title={this.props.title}
              />
              <div>
                <div>
                  <button
                    type="button"
                    className="btn btn-default"
                    onClick={this.cancelForm}
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className={`btn btn-primary ${loading ? "loading" : ""}`}
                    onClick={this.onSaveHandler.bind(this)}
                    disabled={loading}
                  >
                    {loading && (
                      <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    )}
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
Preview.defaultProps = {
  showCorrectColumn: false,
  files: [],
  editMode: false,
  editElement: null,
  className: "react-form-builder-preview",
};
