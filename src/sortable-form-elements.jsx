import SortableElement from './sortable-element';
import PlaceHolder from './form-place-holder';
import BaseFormElements from './form-elements';

const {
  TextInput, NumberInput, TextArea, Dropdown, Checkboxes,
  DatePicker, RadioButtons, FilePicker, Email, Phone
} = BaseFormElements;

const FormElements = {};

FormElements.TextInput = SortableElement(TextInput);
FormElements.Email = SortableElement(Email);
FormElements.Phone = SortableElement(Phone);
FormElements.NumberInput = SortableElement(NumberInput);
FormElements.TextArea = SortableElement(TextArea);
FormElements.Dropdown = SortableElement(Dropdown);
FormElements.Checkboxes = SortableElement(Checkboxes);
FormElements.DatePicker = SortableElement(DatePicker);
FormElements.TimePicker = SortableElement(DatePicker);
FormElements.RadioButtons = SortableElement(RadioButtons);
FormElements.FilePicker = SortableElement(FilePicker);
FormElements.PlaceHolder = SortableElement(PlaceHolder);

export default FormElements;
