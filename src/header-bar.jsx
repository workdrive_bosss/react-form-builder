/**
  * <HeaderBar />
  */

import React from 'react';

export default class HeaderBar extends React.Component {
  render() {
    return (
      <div className="toolbar-header">
        <span className="badge badge-secondary">{this.props.data.text}</span>
        <div className="toolbar-header-buttons">
          {this.props.data.element !== 'LineBreak' &&
            <span className="is-isolated cicon-edit-pencil" onClick={this.props.editModeOn.bind(this.props.parent, this.props.data)}></span>
          }
        <span className="is-isolated cicon-delete" data-target="#deleteModal" role="button" data-toggle="modal" onClick={this.props.onDestroy.bind(this, this.props.data)}></span>
        </div >
      </div >
    );
  }
}
