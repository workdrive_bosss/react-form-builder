// eslint-disable-next-line max-classes-per-file
import React, { useCallback, useState } from "react";
import Select from "react-select";
import xss from "xss";
import { format, parse } from "date-fns";
// import moment from 'moment';
import SignaturePad from "react-signature-canvas";
import ReactBootstrapSlider from "react-bootstrap-slider";
import ReactDatePicker from "react-datepicker";
import StarRating from "./star-rating";
import HeaderBar from "./header-bar";
import Dropzone, { useDropzone } from "react-dropzone";

const FormElements = {};
const myxss = new xss.FilterXSS({
  whiteList: {
    u: [],
    br: [],
    b: [],
    i: [],
    ol: ["style"],
    ul: ["style"],
    li: [],
    p: ["style"],
    sub: [],
    sup: [],
    div: ["style"],
    em: [],
    strong: [],
    span: ["style"],
  },
});

const noData = () => <i className="grey-color">{'No Answer'}</i>

const ComponentLabel = (props) => {
  const hasRequiredLabel =
    props.data.hasOwnProperty("required") &&
    props.data.required === true &&
    !props.read_only;

  return (
    <label className={props.className || ""}>
      <span>{props.data.label}</span>

      {hasRequiredLabel && <span className="label-required ">*</span>}
    </label>
  );
};

const ComponentHeader = (props) => {
  if (props.mutable || props.isReadOnly) {
    return null;
  }
  return (
    <div>
      {props.data.pageBreakBefore && (
        <div className="preview-page-break">Page Break</div>
      )}
      <HeaderBar
        parent={props.parent}
        editModeOn={props.editModeOn}
        data={props.data}
        onDestroy={props._onDestroy}
        onEdit={props.onEdit}
        static={props.data.static}
        required={props.data.required}
      />
    </div>
  );
};

class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.inputField = React.createRef();
  }

  render() {
    const { isReadOnly = false } = this.props
    const props = { placeholder: this.props.data.placeholder };
    props.type = "text";
    props.className = "form-control";
    props.name = this.props.data.field_name;
    if (this.props.mutable) {
      props.defaultValue = this.props.defaultValue;
      props.ref = this.inputField;
    }

    let baseClasses = "SortableItem rfb-item";

    if (this.props.read_only) {
      props.disabled = "disabled";
    }
    const value = props.defaultValue
    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel {...this.props} />
          {!this.props.isPreviewResponses && <input {...props} placeholder={props.placeholder} />}
          {this.props.isPreviewResponses && <div >{value ? value : noData()}</div>}
        </div>
      </div>
    );
  }
}

class Email extends React.Component {
  constructor(props) {
    super(props);
    this.inputField = React.createRef();
  }

  render() {
    const { isReadOnly = false } = this.props
    const props = { placeholder: this.props.data.placeholder };
    props.type = "text";
    props.className = "form-control";
    props.name = this.props.data.field_name;
    if (this.props.mutable) {
      props.defaultValue = this.props.defaultValue;
      props.ref = this.inputField;
    }

    let baseClasses = "SortableItem rfb-item";
    const value = props.defaultValue

    if (this.props.read_only) {
      props.disabled = "disabled";
    }

    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel {...this.props} />
          {!this.props.isPreviewResponses && <input {...props} placeholder={props.placeholder} type="email" />}
          {this.props.isPreviewResponses && <div >{value ? value : noData()}</div>}
        </div>
      </div>
    );
  }
}
class Phone extends React.Component {
  constructor(props) {
    super(props);
    this.inputField = React.createRef();
  }

  render() {
    const { isReadOnly = false } = this.props
    const props = { placeholder: this.props.data.placeholder };
    props.type = "text";
    props.className = "form-control";
    props.name = this.props.data.field_name;
    if (this.props.mutable) {
      props.defaultValue = this.props.defaultValue;
      props.ref = this.inputField;
    }

    let baseClasses = "SortableItem rfb-item";
    const value = props.defaultValue

    if (this.props.read_only) {
      props.disabled = "disabled";
    }

    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel {...this.props} />
          {!this.props.isPreviewResponses && <input {...props} placeholder={props.placeholder} type="tel" />}
          {this.props.isPreviewResponses && <div >{value ? value : noData()}</div>}

        </div>
      </div>
    );
  }
}
class NumberInput extends React.Component {
  constructor(props) {
    super(props);
    this.inputField = React.createRef();
  }

  render() {
    const { isReadOnly = false } = this.props
    const props = { placeholder: this.props.data.placeholder };
    props.type = "number";
    props.className = "form-control";
    props.name = this.props.data.field_name;

    if (this.props.mutable) {
      props.defaultValue = this.props.defaultValue;
      props.ref = this.inputField;
    }

    if (this.props.read_only) {
      props.disabled = "disabled";
    }

    let baseClasses = "SortableItem rfb-item";
    const value = props.defaultValue

    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel {...this.props} />
          {!this.props.isPreviewResponses && <input {...props} />}
          {this.props.isPreviewResponses && <div >{value ? value : noData()}</div>}
        </div>
      </div>
    );
  }
}

class TextArea extends React.Component {
  constructor(props) {
    super(props);
    this.inputField = React.createRef();
  }

  render() {
    const { isReadOnly = false } = this.props
    const props = { placeholder: this.props.data.placeholder };
    props.className = "form-control";
    props.name = this.props.data.field_name;

    if (this.props.read_only) {
      props.disabled = "disabled";
    }

    if (this.props.mutable) {
      props.defaultValue = this.props.defaultValue;
      props.ref = this.inputField;
    }

    const baseClasses = "SortableItem rfb-item";
    const value = props.defaultValue

    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel {...this.props} />
          {!this.props.isPreviewResponses && <input {...props} placeholder={props.placeholder} />}
          {this.props.isPreviewResponses && <div >{value ? value : noData()}</div>}
        </div>
      </div>
    );
  }
}

class DatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.inputField = React.createRef();

    this.updateFormat(props);
    this.state = this.updateDateTime(props, this.formatMask);
  }

  formatMask = "";

  handleChange = (dt) => {
    let placeholder;
    if (dt && dt.target) {
      placeholder =
        dt && dt.target && dt.target.value === ""
          ? this.formatMask.toLowerCase()
          : "";
      // ! format the date once implementing the form submission for bosss
      // const formattedDate = dt.target.value
      //   ? format(dt.target.value, this.formatMask)
      //   : "";
      const formattedDate = dt.target.value
        ? dt.target.value
        : "";
      this.setState({
        value: formattedDate,
        internalValue: formattedDate,
        placeholder,
      });
    } else {
      // ! format the date once implementing the form submission for bosss
      // const newState = {
      //   value: dt ? format(dt, this.formatMask) : "",
      //   internalValue: dt,
      //   placeholder,
      // }
      const newState = {
        value: dt ? dt : "",
        internalValue: dt,
        placeholder,
      }
      this.setState(newState);
    }
  };

  updateFormat(props) {
    const { showTimeSelect, showTimeSelectOnly } = props.data;
    const dateFormat =
      showTimeSelect && showTimeSelectOnly ? "" : props.data.dateFormat;
    const timeFormat = showTimeSelect ? props.data.timeFormat : "";
    const formatMask = `${dateFormat} ${timeFormat}`.trim();
    const updated = formatMask !== this.formatMask;
    this.formatMask = formatMask;
    return updated;
  }

  updateDateTime(props, formatMask) {
    let value;
    let internalValue;
    const { defaultToday } = props.data;
    if (
      defaultToday &&
      (props.defaultValue === "" || props.defaultValue === undefined)
    ) {
      // ! format the date once implementing the form submission for bosss
      // value = format(new Date(), formatMask);
      value = Date()
      internalValue = new Date();
    } else {
      value = props.defaultValue;

      if (value === "" || value === undefined) {
        internalValue = undefined;
      } else {
        // ! format the date once implementing the form submission for bosss
        // internalValue = parse(value, this.formatMask, new Date());

        internalValue = value
      }
    }
    return {
      value,
      internalValue,
      placeholder: formatMask.toLowerCase(),
      defaultToday,
    };
  }

  componentWillReceiveProps(props) {
    const formatUpdated = this.updateFormat(props);
    if (props.data.defaultToday !== !this.state.defaultToday || formatUpdated) {
      const state = this.updateDateTime(props, this.formatMask);
      this.setState(state);
    }
  }

  render() {
    const { showTimeSelect, showTimeSelectOnly } = this.props.data;
    const { isReadOnly = false } = this.props
    const props = {};
    props.type = "date";
    props.className = "form-control";
    props.name = this.props.data.field_name;
    const readOnly = this.props.data.readOnly || this.props.read_only;
    const iOS =
      /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    const placeholderText = this.formatMask.toLowerCase();

    if (this.props.mutable) {
      props.defaultValue = this.props.defaultValue;
      props.ref = this.inputField;
    }

    const baseClasses = "SortableItem rfb-item";
    if (this.props.isPreviewResponses) {
      const value = this.state.value
      return <div >{value ? value : noData()}</div>
    }
    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel {...this.props} />
          <div>
            {readOnly && (
              <input
                type="text"
                name={props.name}
                ref={props.ref}
                readOnly={readOnly}
                placeholder={this.state.placeholder}
                value={this.state.value}
                className="form-control"
              />
            )}
            {iOS && !readOnly && (
              <input
                type="date"
                name={props.name}
                ref={props.ref}
                onChange={this.handleChange}
                dateFormat="MM/DD/YYYY"
                placeholder={this.state.placeholder}
                value={this.state.value}
                className="form-control"
              />
            )}
            {!iOS && !readOnly && (
              <ReactDatePicker
                name={props.name}
                ref={props.ref}
                onChange={this.handleChange}
                // selected={this.state.internalValue}
                todayButton={"Today"}
                className="form-control"
                isClearable={true}
                // showTimeSelect={showTimeSelect}
                // showTimeSelectOnly={showTimeSelectOnly}
                dateFormat={this.formatMask}
                placeholderText={placeholderText}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

class Checkboxes extends React.Component {
  constructor(props) {
    super(props);
    this.options = {};
  }

  error = ''

  render() {
    const self = this;
    const { isQuiz = false, isReadOnly = false } = this.props;

    let classNames = "custom-control custom-checkbox";
    if (this.props.data.inline) {
      classNames += " option-inline";
    }
    this.error = '';
    if (isQuiz) {
      let inCorrect = true;
      this.props.data.options.forEach(currentOption => {
        if (currentOption.correct) {
          inCorrect = false;
        }
      })
      if (inCorrect) {
        this.error = 'No correct answer is picked';
      }
    }

    const baseClasses = "SortableItem rfb-item";
    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel className="form-label" {...this.props} />
          {this.props.data.options.map((option) => {
            const this_key = `preview_${option.key}`;
            const props = {};
            props.name = `option_${option.key}`;

            props.type = "checkbox";
            props.value = option.value;
            if (self.props.mutable) {
              props.defaultChecked =
                self.props.defaultValue !== undefined &&
                self.props.defaultValue.indexOf(option.key) > -1;
            }
            if (this.props.read_only) {
              props.disabled = "disabled";
            }
            return (
              <div className={classNames} key={this_key}>
                <input
                  id={"fid_" + this_key}
                  className="custom-control-input"
                  ref={(c) => {
                    if (c && self.props.mutable) {
                      self.options[`child_ref_${option.key}`] = c;
                    }
                  }}
                  {...props}
                />
                <label
                  className="custom-control-label"
                  htmlFor={"fid_" + this_key}
                >
                  {option.text}
                </label>
                {option.correct && <i style={{ color: 'green' }} className="ml-3 fas fa-check-circle" />}
              </div>
            );
          })}
          {this.error !== '' && <label className="mt-1 text-danger">{this.error}</label>}
        </div>
      </div>
    );
  }
}

class RadioButtons extends React.Component {
  constructor(props) {
    super(props);
    this.options = {};
  }

  render() {
    const self = this;
    const { isQuiz = false, isReadOnly = false } = this.props;
    let classNames = "custom-control custom-radio";
    if (this.props.data.inline) {
      classNames += " option-inline";
    }

    const baseClasses = "SortableItem rfb-item";
    let error = '';
    if (isQuiz) {
      let inCorrect = true;
      this.props.data.options.forEach(currentOption => {
        if (currentOption.correct) {
          inCorrect = false;
        }
      })
      if (inCorrect) {
        error = 'No correct answer is picked';
      }
    }

    return (
      <div className={baseClasses}>
        <ComponentHeader {...this.props} isReadOnly={isReadOnly} />
        <div className="form-group">
          <ComponentLabel className="form-label" {...this.props} />
          {this.props.data.options.map((option) => {
            const this_key = `preview_${option.key}`;
            const props = {};
            props.name = self.props.data.field_name;

            props.type = "radio";
            props.value = option.value;
            if (self.props.mutable) {
              props.defaultChecked =
                self.props.defaultValue !== undefined &&
                (self.props.defaultValue.indexOf(option.key) > -1 ||
                  self.props.defaultValue.indexOf(option.value) > -1);
            }
            if (this.props.read_only) {
              props.disabled = "disabled";
            }
            return (
              <div className={classNames} key={this_key}>
                <input
                  id={"fid_" + this_key}
                  className="custom-control-input"
                  ref={(c) => {
                    if (c && self.props.mutable) {
                      self.options[`child_ref_${option.key}`] = c;
                    }
                  }}
                  {...props}
                />
                <label
                  className="custom-control-label"
                  htmlFor={"fid_" + this_key}
                >
                  {option.text}
                </label>
                {option.correct && <i style={{ color: 'green' }} className="ml-3 fas fa-check-circle" />}
              </div>

            );
          })}
          {error !== '' && <label className="mt-1 text-danger">{error}</label>}
        </div>
      </div>
    );
  }
}

class FilePicker extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  state = {
    uploadedFile: null,
  };

  render() {
    const props = {};
    props.name = this.props.data.field_name;
    if (this.props.mutable) {
      props.ref = this.ref;
    }
    const baseClasses = "SortableItem rfb-item";

    const { uploadedFile } = this.state;

    return (
      <Dropzone
        onDrop={(acceptedFiles) => {
          acceptedFiles.forEach((file) => {
            const reader = new FileReader();
            reader.onabort = () => console.log("file reading was aborted");
            reader.onerror = () => console.log("file reading has failed");
            reader.onload = () => {
              // Do whatever you want with the file contents
              const binaryStr = reader.result;
              this.setState({ uploadedFile: file });
            };
            reader.readAsArrayBuffer(file);
          });
        }}
      >
        {({ getRootProps, getInputProps }) => (
          <div className={baseClasses}>
            <ComponentHeader {...this.props} />
            <div className="form-group">
              <ComponentLabel {...this.props} />
              <div
                {...getRootProps()}
                className="react-dropzone"
                ref={this.ref}
              >
                <input {...getInputProps()} />
                {uploadedFile && <p>{`${uploadedFile.name}`}</p>}
                <button
                  className="btn btn-primary m-8"
                  onClick={(e) => {
                    e.preventDefault();
                  }}
                >
                  {"Upload"}
                </button>
              </div>
            </div>
          </div>
        )}
      </Dropzone>
      // <div className={baseClasses}>
      //   <ComponentHeader {...props} />
      //   <div className="form-group">

      //     <div {...getRootProps()} className="d-flex" >
      //       <input {...getInputProps()} />
      //       <p>{`${uploadedFile ? uploadedFile.name : ''}`}</p>
      //       <button className="btn btn-primary m-8" onClick={(e) => { e.preventDefault(); }}>{'Upload'}</button>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

FormElements.TextInput = TextInput;
FormElements.NumberInput = NumberInput;
FormElements.TextArea = TextArea;
FormElements.Checkboxes = Checkboxes;
FormElements.DatePicker = DatePicker;
FormElements.TimePicker = DatePicker;
FormElements.RadioButtons = RadioButtons;
FormElements.FilePicker = FilePicker;
FormElements.Email = Email;
FormElements.Phone = Phone;

export default FormElements;
