import React from "react";
import TextAreaAutosize from "react-textarea-autosize";
import { ContentState, EditorState, convertFromHTML } from "draft-js";

import DynamicOptionList from "./dynamic-option-list";
import { get } from "./stores/requests";
import ID from "./UUID";
import { TOOLBOX_ITEMS_KEYS } from "./constants";

const toolbar = {
  options: ["inline", "list", "textAlign", "fontSize", "link", "history"],
  inline: {
    inDropdown: false,
    className: undefined,
    options: ["bold", "italic", "underline", "superscript", "subscript"],
  },
};

export default class FormElementsEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      element: this.props.element,
      initElement: this.props.element,
      data: this.props.data,
      dirty: false,
    };
  }

  toggleRequired() {
    // const this_element = this.state.element;
  }

  editElementProp(elemProperty, targProperty, e) {
    // elemProperty could be content or label
    // targProperty could be value or checked
    const this_element = this.state.element;
    this_element[elemProperty] = e.target[targProperty];

    this.setState(
      {
        element: this_element,
        dirty: true,
      },
      () => {
        if (targProperty === "checked") {
          this.updateElement();
        }
      }
    );
  }

  updateElement() {
    const this_element = this.state.element;
    // to prevent ajax calls with no change
    if (this.state.dirty) {
      // this.props.updateElement.call(this.props.preview, this_element);
      this.setState({ dirty: false, element: this_element });
    }
  }

  convertFromHTML(content) {
    const newContent = convertFromHTML(content);
    if (!newContent.contentBlocks || !newContent.contentBlocks.length) {
      // to prevent crash when no contents in editor
      return EditorState.createEmpty();
    }
    const contentState = ContentState.createFromBlockArray(newContent);
    return EditorState.createWithContent(contentState);
  }

  addOptions() {
    const optionsApiUrl = document.getElementById("optionsApiUrl").value;
    if (optionsApiUrl) {
      get(optionsApiUrl).then((data) => {
        this.props.element.options = [];
        const { options } = this.props.element;
        data.forEach((x) => {
          // eslint-disable-next-line no-param-reassign
          x.key = ID.uuid();
          options.push(x);
        });
        const this_element = this.state.element;
        this.setState({
          element: this_element,
          dirty: true,
        });
      });
    }
  }

  render() {
    const { isQuiz = false } = this.props
    if (this.state.dirty) {
      this.props.element.dirty = true;
    }

    const this_checked = this.props.element.hasOwnProperty("required")
      ? this.props.element.required
      : false;
    const this_read_only = this.props.element.hasOwnProperty("readOnly")
      ? this.props.element.readOnly
      : false;
    const this_default_today = this.props.element.hasOwnProperty("defaultToday")
      ? this.props.element.defaultToday
      : false;
    const this_show_time_select = this.props.element.hasOwnProperty(
      "showTimeSelect"
    )
      ? this.props.element.showTimeSelect
      : false;
    const this_show_time_select_only = this.props.element.hasOwnProperty(
      "showTimeSelectOnly"
    )
      ? this.props.element.showTimeSelectOnly
      : false;
    const this_checked_inline = this.props.element.hasOwnProperty("inline")
      ? this.props.element.inline
      : false;
    const this_checked_bold = this.props.element.hasOwnProperty("bold")
      ? this.props.element.bold
      : false;
    const this_checked_italic = this.props.element.hasOwnProperty("italic")
      ? this.props.element.italic
      : false;
    // const this_checked_center = this.props.element.hasOwnProperty('center') ? this.props.element.center : false;

    const {
      canHaveDisplayHorizontal,
      canHaveOptionCorrect,
      canHaveOptionValue,
    } = this.props.element;

    const this_files = this.props.files.length ? this.props.files : [];
    if (
      this_files.length < 1 ||
      (this_files.length > 0 && this_files[0].id !== "")
    ) {
      this_files.unshift({ id: "", file_name: "" });
    }
    return (
      <div id="deleteModal" className="modal show d-block" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content d-flex flex-col">
            <div>
              <div className="clearfix">
                <i className="float-right fas fa-times dismiss-edit" onClick={this.props.manualEditModeOff}></i>
              </div>
              {/* {this.props.element.hasOwnProperty('content') &&
          <div className="form-group">
            <label className="control-label">Text to display:</label>
            <input className="form-control" value={this.props.element.content} onChange={this.editElementProp.bind(this, 'content', 'value')} />
          </div>
        } */}
              {this.props.element.hasOwnProperty("href") && (
                <div className="form-group">
                  <TextAreaAutosize
                    type="text"
                    className="form-control"
                    defaultValue={this.props.element.href}
                    onBlur={this.updateElement.bind(this)}
                    onChange={this.editElementProp.bind(this, "href", "value")}
                  />
                </div>
              )}
              {this.props.element.hasOwnProperty("label") && (
                <div className="form-group">
                  <label>{isQuiz ? 'Question' : 'Label'}</label>
                  <input
                    className="form-control"
                    value={this.props.element.label}
                    onChange={this.editElementProp.bind(this, "label", "value")}
                  />
                  <br />
                  {/* {this.props.element.hasOwnProperty("placeholder") && (
                    <>
                      <label className="font-weight-bold">Placeholder</label>
                      <input
                        className="form-control"
                        value={this.props.element.placeholder}
                        onChange={this.editElementProp.bind(
                          this,
                          "placeholder",
                          "value"
                        )}
                      />
                      <br />
                    </>
                  )} */}

                  {/* {this.props.element.hasOwnProperty('readOnly') &&
              <div className="custom-control custom-checkbox">
                <input id="is-read-only" className="custom-control-input" type="checkbox" checked={this_read_only} value={true} onChange={this.editElementProp.bind(this, 'readOnly', 'checked')} />
                <label className="custom-control-label" htmlFor="is-read-only">
                  Read only
                </label>
              </div>
            }
            {this.props.element.hasOwnProperty('defaultToday') &&
              <div className="custom-control custom-checkbox">
                <input id="is-default-to-today" className="custom-control-input" type="checkbox" checked={this_default_today} value={true} onChange={this.editElementProp.bind(this, 'defaultToday', 'checked')} />
                <label className="custom-control-label" htmlFor="is-default-to-today">
                  Default to Today?
                </label>
              </div>
            } */}
                  {/* {this.props.element.hasOwnProperty('showTimeSelect') &&
                    this.props.element.element ===
                    TOOLBOX_ITEMS_KEYS.DATEPICKER && (
                      <div className="custom-control custom-checkbox">
                      <input
                        id="show-time-select"
                        className="custom-control-input"
                        type="checkbox"
                        checked={this_show_time_select}
                        value={true}
                        onChange={this.editElementProp.bind(
                          this,
                          'showTimeSelect',
                          'checked',
                        )}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="show-time-select"
                      >
                          Show Time Select?
                        </label>
                      </div>
                    )} */}
                  {/* {this_show_time_select && this.props.element.hasOwnProperty('showTimeSelectOnly') &&
              <div className="custom-control custom-checkbox">
                <input id="show-time-select-only" className="custom-control-input" type="checkbox" checked={this_show_time_select_only} value={true} onChange={this.editElementProp.bind(this, 'showTimeSelectOnly', 'checked')} />
                <label className="custom-control-label" htmlFor="show-time-select-only">
                  Show Time Select Only?
                </label>
              </div>
            } */}
                  {/*   {(this.state.element.element === "RadioButtons" ||
                    this.state.element.element === "Checkboxes") &&
                    canHaveDisplayHorizontal && (
                      <div className="custom-control custom-checkbox">
                      <input
                        id="display-horizontal"
                        className="custom-control-input"
                        type="checkbox"
                        checked={this_checked_inline}
                        value={true}
                        onChange={this.editElementProp.bind(
                          this,
                          "inline",
                          "checked"
                        )}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="display-horizontal"
                      >
                          Display horizonal
                        </label>
                      </div>
                  )
                    } */}
                </div>
              )}
              {/*  {this.props.element.hasOwnProperty('default_value') && (
                <div className="form-group">
                  <div className="form-group-range">
                    <label className="control-label" htmlFor="defaultSelected">
                      Default Selected
                    </label>
                    <input
                      id="defaultSelected"
                      type="number"
                      className="form-control"
                      defaultValue={this.props.element.default_value}
                      onBlur={this.updateElement.bind(this)}
                      onChange={this.editElementProp.bind(
                        this,
                        'default_value',
                        'value',
                      )}
                    />
                  </div>
                </div>
              )}
              {this.props.element.hasOwnProperty('static') &&
                this.props.element.static && (
                  <div className="form-group">
                    <label className="control-label">Text Style</label>
                    <div className="custom-control custom-checkbox">
                      <input
                        id="do-bold"
                        className="custom-control-input"
                        type="checkbox"
                        checked={this_checked_bold}
                        value={true}
                        onChange={this.editElementProp.bind(
                          this,
                          'bold',
                          'checked',
                        )}
                      />
                      <label className="custom-control-label" htmlFor="do-bold">
                        Bold
                      </label>
                    </div>
                    <div className="custom-control custom-checkbox">
                      <input
                        id="do-italic"
                        className="custom-control-input"
                        type="checkbox"
                        checked={this_checked_italic}
                        value={true}
                        onChange={this.editElementProp.bind(
                          this,
                          'italic',
                          'checked',
                        )}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="do-italic"
                      >
                        Italic
                      </label>
                    </div>
                  </div>
              )}
              {this.props.showCorrectColumn &&
                this.props.element.canHaveAnswer &&
                !this.props.element.hasOwnProperty('options') && (
                  <div className="form-group">
                    <label className="control-label" htmlFor="correctAnswer">
                      Correct Answer
                    </label>
                    <input
                      id="correctAnswer"
                      type="text"
                      className="form-control"
                      defaultValue={this.props.element.correct}
                      onBlur={this.updateElement.bind(this)}
                      onChange={this.editElementProp.bind(
                        this,
                        'correct',
                        'value',
                      )}
                    />
                  </div>
              )} */}

              {this.props.element.hasOwnProperty("options") && (
                <DynamicOptionList
                  showCorrectColumn={this.props.showCorrectColumn}
                  canHaveOptionCorrect={canHaveOptionCorrect}
                  canHaveOptionValue={canHaveOptionValue}
                  data={this.props.preview.state.data}
                  updateElement={this.props.updateElement}
                  preview={this.props.preview}
                  element={this.props.element}
                  key={this.props.element.options.length}
                  isQuiz={this.props.isQuiz}
                />
              )}
              {!isQuiz && <div className="custom-control custom-checkbox mt-2">
                <input
                  id="is-required"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_checked}
                  value={true}
                  onChange={this.editElementProp.bind(
                    this,
                    "required",
                    "checked"
                  )}
                />
                <label className="custom-control-label" htmlFor="is-required">
                  Required
                </label>
              </div>}
              <div className="bottom-action-container">
                <button
                  type="button"
                  className="btn btn-default"
                  onClick={() => {
                    this.setState({ element: this.state.initElement }, () => {
                      this.props.manualEditModeOff();
                    });
                  }}

                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.props.manualEditModeOff}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
FormElementsEdit.defaultProps = { className: "edit-element-fields" };
